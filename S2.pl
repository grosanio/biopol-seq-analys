#!/usr/bin/perl
#merge.plx
#merge two stripped-prep'ed files into one file formated as:
#	beadcall-id
#	seq1
#	seq2
# - second program
use warnings;
use strict;
use 5.010;
 
use FileHandle;

print "Enter a pair of stripper-formated DNA files (\"stripped_example.txt stripped_another.txt\"): ";

my $files = <STDIN>;
chomp $files;
my @input = split/ /, $files;

my $check = @input;
unless ($check == 2) {
	die "ERROR: Incorrect number of files.\n";
}

my ($seq_1, $seq_2, $seq_out) = (0, 0, 0);

# input files
my $data_1 = FileHandle->new("$input[0]") or die "ERROR: $!";
my $data_2 = FileHandle->new("$input[1]") or die "ERROR: $!";
 
# output file
my $merge = FileHandle->new(">merged_$input[0]") or die "ERROR: $!";
 
while (!$data_1->eof() || !$data_2->eof()){
		my $l1 = $data_1->getline();
		$seq_1++;
		my $l2 = $data_2->getline();
		$seq_2++;
		my @line_1 = split/\t/,$l1;
		my $sequence_1 = pop @line_1;
		my $beadid_1 = pop @line_1;
		chomp $sequence_1;
		my @line_2 = split/\t/,$l2;
		my $sequence_2 = pop @line_2;
		my $beadid_2 = pop @line_2;
		if ($beadid_2 eq $beadid_1) {
			$merge->print("$beadid_1\t$sequence_1\t$sequence_2");
			$seq_out++;
		}
}

print "$seq_1 sequences found in $input[0].\n$seq_2 sequences found in $input[1].\n$seq_out sequences merged together in merged_$input[0].\n";


$data_1->close();
$data_2->close();
$merge->close(); 