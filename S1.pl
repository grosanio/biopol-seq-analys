#!/usr/bin/perl
#stripflip.plx
#strips solexa data of quality score leaving one call bead line and its sequence only (removes quality score) and returns the reverse compliment of the second file - first program
use warnings;
use strict;
use 5.010;

print "Enter pairs of raw solexa files (\"FIRSTexample.txt FIRSTexamplepair.txt SECONDexample.txt SECONDexamplepair.txt\"): ";

my $bad = 0;
my $badfwd = 0;
my $badrvs = 0;
my ($beadhold, $seqhold);
my $beadcount = 0;
my $seqcount = 0;
my $count = 0;
my $files = <STDIN>;
chomp $files;
my @input = split / /, $files;

my $check = my $total = @input;
unless (($check % 2) == 0) {
	die "ERROR: Incorrect number of files.\n";
}

open OUTFWD, ">", "stripped_$input[0]";
open BADFWD, ">", "false_$input[0]";
open OUTRVS, ">", "stripped_$input[1]";
open BADRVS, ">", "false_$input[1]";

for $check (@input) {
	$count++;
	my $holder = $check;
	open DATA, "<", "$holder" or die "ERROR: No file(s) of that name exists in this folder.\n";

	while (<DATA>) {
		chomp;
		if (($count % 2) != 0) {
			if (/^\@HS/) {
				$beadcount++;
				$beadhold = $_;
				s/@//g;
				s/\s\d:\w:\d://g;
				s/[:#]/_/g;
				print OUTFWD "$_"."\t";
			} elsif ($_ !~ /[\W\dEFIJLOPQUXZa-z_]/) {
				$seqcount++;
				$seqhold = $_;
				if ($seqcount == $beadcount) {
					print OUTFWD "$_"."\n";
				} else {
					$bad++;
					$badfwd++;
					print BADFWD "$bad\t\t$_\n";
					$seqcount--;
				}
			} 
		} elsif (($count % 2) == 0) {
			if (/^\@HS/) {
				$beadcount++;
				$beadhold = $_;
				s/@//g;
				s/\s\d:\w:\d://g;
				s/[:#]/_/g;
				print OUTRVS "$_"."\t";
			} elsif ($_ !~ /[\W\dEFIJLOPQUXZa-z_]/) {
				$seqcount++;
				$seqhold = $_;
				if ($seqcount == $beadcount) {
					tr/ACTG/TGAC/;
					$_ = scalar reverse $_;
					print OUTRVS "$_"."\n";
				} else {
					$bad++;
					$badrvs++;
					print BADRVS "$bad\t\t$_\n";
					$seqcount--;
				}
			}
		}
	}
	if (($count % 2) != 0) {
		print "Output saved as stripped_$input[0]. There were $beadcount beads and $seqcount sequences and $bad false calls saved to file false_$input[0].\n";
	}else{
		print "Output saved as stripped_$input[1]. There were $beadcount beads and $seqcount sequences and $bad false calls saved to file false_$input[1].\n";
	}
	$beadcount = $seqcount = 0;
	$bad = 0;
	close DATA;
}

close OUTFWD;
close OUTRVS;
close BADFWD;
if ($badfwd == 0) {
	unlink ("false_$input[0]");
}
close BADRVS;
if ($badrvs == 0) {
	unlink ("false_$input[1]");
}