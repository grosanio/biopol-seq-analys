#!/usr/bin/perl
use warnings;
use strict;

my $glo0 = "[A|T][A|T]";

print "Enter sequence-counted files (\"seqcount_contig_136_contig_136_FIRSTexample.txt seqcount_contig_136_contig_136_SECONDexample.txt seqcount_contig_136_contig_136_THIRDexample.txt\"): ";


my $files = <STDIN>;
chomp $files;
my @input = split / /, $files;
my $name = $input[0];
my $tracker = 0;
#my %atgapsO = ('sequences without A/T dinucleotide', 0, 'sequences with only one A/T dinucleotide', 0);
my %atgapsN = ('sequences without A/T dinucleotide', 0, 'sequences with only one A/T dinucleotide', 0);


for my $file (@input) {
	open DATA, "<", "$file" or die "ERROR: $file does not exists in this folder.\n";
	close DATA;
}

#open OUTO, ">", "atgap_overlaps_separated_$name";
#print OUTO "Sequence\tNearest Next Dinucleotide\n";
open OUTN, ">", "atgap_nonoverlaps_separated_$name";
print OUTN "Sequence\tNearest Next Dinucleotide\n";

for my $file (@input) {
	open DATA, "<", "$file" or die "ERROR: No file(s) of that name exists in this folder.\n";
	while (<DATA>) {
#		my $goneO = 0;
		my $goneN = -1;
		my @line = split/\s/;
		my $reps = $line[1];
		chomp $reps;
		$tracker = $tracker + $reps;
		my $sequence = $line[0]; #split input line into copies of unique sequence and number of times represented in data
#		print OUTO "$sequence\n";
		print OUTN "$sequence\t";
		my $holdO = my $holdN = $sequence;
#		my @foundO;
		my @foundN;
#		while ($holdO =~ /$glo0/){
#			my $removeO = $-[0] + 1;
#			$goneO = $goneO + $removeO;
#			push @foundO, $goneO;
#			$holdO =~ s/^.{$removeO}//;
#		}
		while ($holdN =~ /$glo0/){
			my $removeN = $-[0] + 2;
			$goneN = $goneN + $removeN;
			push @foundN, $goneN;			
			$holdN =~ s/^.{$removeN}//;
		}
#		my $numberO = scalar(@foundO);
		my $numberN = scalar(@foundN);
#		my @distanceO;
		my @distanceN;
#		if ($numberO == 1 ) {
#			$atgapsO{'sequences with only one A/T dinucleotide'} = $atgapsO{'sequences with only one A/T dinucleotide'} + $reps;
#		} elsif ($numberO == 0 ) {
#			$atgapsO{'sequences without A/T dinucleotide'} = $atgapsO{'sequences without A/T dinucleotide'} + $reps;
#		} else {
#			while ($numberO > 1){
#				my $largest = pop @foundO;
#				my $next = $foundO[$#foundO];
#				my $difference = $largest - $next;
#				push @distanceO, $difference;
#				$numberO--;
#			}
#		}
#		for (@distanceO) {
#			if (exists($atgapsO{$_})) {
#				$atgapsO{$_} = $atgapsO{$_} + $reps;
#			}else {
#				$atgapsO{$_} = $reps;
#			}
#		}
		if ($numberN == 1 ) {
			$atgapsN{'sequences with only one A/T dinucleotide'} = $atgapsN{'sequences with only one A/T dinucleotide'} + $reps;
		} elsif ($numberN == 0 ) {
			$atgapsN{'sequences without A/T dinucleotide'} = $atgapsN{'sequences without A/T dinucleotide'} + $reps;
		} else {
			while ($numberN > 1){
				my $largest = pop @foundN;
				my $next = $foundN[$#foundN];
				my $difference = $largest - $next;
				push @distanceN, $difference;
				$numberN--;
			}
		}
		for (0..($reps-1)) {
#			print OUTO "@distanceO\n";
			print OUTN "@distanceN\n";
		}
		for (@distanceN) {
			if (exists($atgapsN{$_})) {
				$atgapsN{$_} = $atgapsN{$_} + $reps;
			}else {
				$atgapsN{$_} = $reps;
			}
		}
		if ($tracker % 10000 == 0) {
			print "$tracker\n";
		}
	}
}

#close OUTO;
close OUTN;

#open BRIEFO, ">", "atgap_overlaps_all_motifpermolecule_$name";
#print BRIEFO "Overlapping Gap\tOccurrences\n";
#for my $key ( sort keys %atgapsO) {
#           print BRIEFO "$key\t$atgapsO{$key}\n";
#}
#close BRIEFO;

open BRIEFN, ">", "atgap_nonoverlaps_all_motifpermolecule_$name";
print BRIEFN "Non-Overlapping Gap\tOccurrences\n";
for my $key ( sort keys %atgapsN) {
           print BRIEFN "$key\t$atgapsN{$key}\n";
}
close BRIEFN;