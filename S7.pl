#!/usr/bin/perl
#baser.plx
#takes combed control and selected files and produces files with percents, t-values and corresponding one-tail p-values
use warnings;
use strict;
use 5.010;
use FileHandle;

my ($control_hold, $selection_hold);
my ($control_1_position, $control_2_position, $control_3_position, $control_4_position, $control_5_position, $control_6_position, $control_7_position, $control_8_position, $control_9_position, $control_10_position, $control_11_position, $control_12_position, $control_13_position, $control_14_position, $control_15_position, $control_16_position, $control_17_position, $control_18_position, $control_19_position, $control_20_position, $control_21_position, $control_22_position, $control_23_position, $control_24_position, $control_25_position, $control_26_position, $control_27_position, $control_28_position, $control_29_position, $control_30_position, $control_31_position, $control_32_position, $control_33_position, $control_34_position, $control_35_position, $control_36_position, $control_37_position, $control_38_position, $control_39_position, $control_40_position, $control_41_position, $control_42_position, $control_43_position, $control_44_position, $control_45_position, $control_46_position, $control_47_position, $control_48_position, $control_49_position, $control_50_position, $control_51_position, $control_52_position, $control_53_position, $control_54_position, $control_55_position, $control_56_position, $control_57_position, $control_58_position, $control_59_position, $control_60_position, $control_61_position, $control_62_position, $control_63_position, $control_64_position, $control_65_position, $control_66_position, $control_67_position, $control_68_position, $control_69_position, $control_70_position, $control_71_position, $control_72_position, $control_73_position, $control_74_position, $control_75_position, $control_76_position, $control_77_position, $control_78_position, $control_79_position, $control_80_position, $control_81_position, $control_82_position, $control_83_position, $control_84_position, $control_85_position, $control_86_position, $control_87_position, $control_88_position, $control_89_position, $control_90_position, $control_total_position, $selection_1_position, $selection_2_position, $selection_3_position, $selection_4_position, $selection_5_position, $selection_6_position, $selection_7_position, $selection_8_position, $selection_9_position, $selection_10_position, $selection_11_position, $selection_12_position, $selection_13_position, $selection_14_position, $selection_15_position, $selection_16_position, $selection_17_position, $selection_18_position, $selection_19_position, $selection_20_position, $selection_21_position, $selection_22_position, $selection_23_position, $selection_24_position, $selection_25_position, $selection_26_position, $selection_27_position, $selection_28_position, $selection_29_position, $selection_30_position, $selection_31_position, $selection_32_position, $selection_33_position, $selection_34_position, $selection_35_position, $selection_36_position, $selection_37_position, $selection_38_position, $selection_39_position, $selection_40_position, $selection_41_position, $selection_42_position, $selection_43_position, $selection_44_position, $selection_45_position, $selection_46_position, $selection_47_position, $selection_48_position, $selection_49_position, $selection_50_position, $selection_51_position, $selection_52_position, $selection_53_position, $selection_54_position, $selection_55_position, $selection_56_position, $selection_57_position, $selection_58_position, $selection_59_position, $selection_60_position, $selection_61_position, $selection_62_position, $selection_63_position, $selection_64_position, $selection_65_position, $selection_66_position, $selection_67_position, $selection_68_position, $selection_69_position, $selection_70_position, $selection_71_position, $selection_72_position, $selection_73_position, $selection_74_position, $selection_75_position, $selection_76_position, $selection_77_position, $selection_78_position, $selection_79_position, $selection_80_position, $selection_81_position, $selection_82_position, $selection_83_position, $selection_84_position, $selection_85_position, $selection_86_position, $selection_87_position, $selection_88_position, $selection_89_position, $selection_90_position, $selection_total_position) = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my ($control_1_sum, $control_2_sum, $control_3_sum, $control_4_sum, $control_5_sum, $control_6_sum, $control_7_sum, $control_8_sum, $control_9_sum, $control_10_sum, $control_11_sum, $control_12_sum, $control_13_sum, $control_14_sum, $control_15_sum, $control_16_sum, $control_17_sum, $control_18_sum, $control_19_sum, $control_20_sum, $control_21_sum, $control_22_sum, $control_23_sum, $control_24_sum, $control_25_sum, $control_26_sum, $control_27_sum, $control_28_sum, $control_29_sum, $control_30_sum, $control_31_sum, $control_32_sum, $control_33_sum, $control_34_sum, $control_35_sum, $control_36_sum, $control_37_sum, $control_38_sum, $control_39_sum, $control_40_sum, $control_41_sum, $control_42_sum, $control_43_sum, $control_44_sum, $control_45_sum, $control_46_sum, $control_47_sum, $control_48_sum, $control_49_sum, $control_50_sum, $control_51_sum, $control_52_sum, $control_53_sum, $control_54_sum, $control_55_sum, $control_56_sum, $control_57_sum, $control_58_sum, $control_59_sum, $control_60_sum, $control_61_sum, $control_62_sum, $control_63_sum, $control_64_sum, $control_65_sum, $control_66_sum, $control_67_sum, $control_68_sum, $control_69_sum, $control_70_sum, $control_71_sum, $control_72_sum, $control_73_sum, $control_74_sum, $control_75_sum, $control_76_sum, $control_77_sum, $control_78_sum, $control_79_sum, $control_80_sum, $control_81_sum, $control_82_sum, $control_83_sum, $control_84_sum, $control_85_sum, $control_86_sum, $control_87_sum, $control_88_sum, $control_89_sum, $control_90_sum, $control_total_sum, $selection_1_sum, $selection_2_sum, $selection_3_sum, $selection_4_sum, $selection_5_sum, $selection_6_sum, $selection_7_sum, $selection_8_sum, $selection_9_sum, $selection_10_sum, $selection_11_sum, $selection_12_sum, $selection_13_sum, $selection_14_sum, $selection_15_sum, $selection_16_sum, $selection_17_sum, $selection_18_sum, $selection_19_sum, $selection_20_sum, $selection_21_sum, $selection_22_sum, $selection_23_sum, $selection_24_sum, $selection_25_sum, $selection_26_sum, $selection_27_sum, $selection_28_sum, $selection_29_sum, $selection_30_sum, $selection_31_sum, $selection_32_sum, $selection_33_sum, $selection_34_sum, $selection_35_sum, $selection_36_sum, $selection_37_sum, $selection_38_sum, $selection_39_sum, $selection_40_sum, $selection_41_sum, $selection_42_sum, $selection_43_sum, $selection_44_sum, $selection_45_sum, $selection_46_sum, $selection_47_sum, $selection_48_sum, $selection_49_sum, $selection_50_sum, $selection_51_sum, $selection_52_sum, $selection_53_sum, $selection_54_sum, $selection_55_sum, $selection_56_sum, $selection_57_sum, $selection_58_sum, $selection_59_sum, $selection_60_sum, $selection_61_sum, $selection_62_sum, $selection_63_sum, $selection_64_sum, $selection_65_sum, $selection_66_sum, $selection_67_sum, $selection_68_sum, $selection_69_sum, $selection_70_sum, $selection_71_sum, $selection_72_sum, $selection_73_sum, $selection_74_sum, $selection_75_sum, $selection_76_sum, $selection_77_sum, $selection_78_sum, $selection_79_sum, $selection_80_sum, $selection_81_sum, $selection_82_sum, $selection_83_sum, $selection_84_sum, $selection_85_sum, $selection_86_sum, $selection_87_sum, $selection_88_sum, $selection_89_sum, $selection_90_sum, $selection_total_sum) = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my ($control_1_proportion, $control_2_proportion, $control_3_proportion, $control_4_proportion, $control_5_proportion, $control_6_proportion, $control_7_proportion, $control_8_proportion, $control_9_proportion, $control_10_proportion, $control_11_proportion, $control_12_proportion, $control_13_proportion, $control_14_proportion, $control_15_proportion, $control_16_proportion, $control_17_proportion, $control_18_proportion, $control_19_proportion, $control_20_proportion, $control_21_proportion, $control_22_proportion, $control_23_proportion, $control_24_proportion, $control_25_proportion, $control_26_proportion, $control_27_proportion, $control_28_proportion, $control_29_proportion, $control_30_proportion, $control_31_proportion, $control_32_proportion, $control_33_proportion, $control_34_proportion, $control_35_proportion, $control_36_proportion, $control_37_proportion, $control_38_proportion, $control_39_proportion, $control_40_proportion, $control_41_proportion, $control_42_proportion, $control_43_proportion, $control_44_proportion, $control_45_proportion, $control_46_proportion, $control_47_proportion, $control_48_proportion, $control_49_proportion, $control_50_proportion, $control_51_proportion, $control_52_proportion, $control_53_proportion, $control_54_proportion, $control_55_proportion, $control_56_proportion, $control_57_proportion, $control_58_proportion, $control_59_proportion, $control_60_proportion, $control_61_proportion, $control_62_proportion, $control_63_proportion, $control_64_proportion, $control_65_proportion, $control_66_proportion, $control_67_proportion, $control_68_proportion, $control_69_proportion, $control_70_proportion, $control_71_proportion, $control_72_proportion, $control_73_proportion, $control_74_proportion, $control_75_proportion, $control_76_proportion, $control_77_proportion, $control_78_proportion, $control_79_proportion, $control_80_proportion, $control_81_proportion, $control_82_proportion, $control_83_proportion, $control_84_proportion, $control_85_proportion, $control_86_proportion, $control_87_proportion, $control_88_proportion, $control_89_proportion, $control_90_proportion, $control_total_proportion, $selection_1_proportion, $selection_2_proportion, $selection_3_proportion, $selection_4_proportion, $selection_5_proportion, $selection_6_proportion, $selection_7_proportion, $selection_8_proportion, $selection_9_proportion, $selection_10_proportion, $selection_11_proportion, $selection_12_proportion, $selection_13_proportion, $selection_14_proportion, $selection_15_proportion, $selection_16_proportion, $selection_17_proportion, $selection_18_proportion, $selection_19_proportion, $selection_20_proportion, $selection_21_proportion, $selection_22_proportion, $selection_23_proportion, $selection_24_proportion, $selection_25_proportion, $selection_26_proportion, $selection_27_proportion, $selection_28_proportion, $selection_29_proportion, $selection_30_proportion, $selection_31_proportion, $selection_32_proportion, $selection_33_proportion, $selection_34_proportion, $selection_35_proportion, $selection_36_proportion, $selection_37_proportion, $selection_38_proportion, $selection_39_proportion, $selection_40_proportion, $selection_41_proportion, $selection_42_proportion, $selection_43_proportion, $selection_44_proportion, $selection_45_proportion, $selection_46_proportion, $selection_47_proportion, $selection_48_proportion, $selection_49_proportion, $selection_50_proportion, $selection_51_proportion, $selection_52_proportion, $selection_53_proportion, $selection_54_proportion, $selection_55_proportion, $selection_56_proportion, $selection_57_proportion, $selection_58_proportion, $selection_59_proportion, $selection_60_proportion, $selection_61_proportion, $selection_62_proportion, $selection_63_proportion, $selection_64_proportion, $selection_65_proportion, $selection_66_proportion, $selection_67_proportion, $selection_68_proportion, $selection_69_proportion, $selection_70_proportion, $selection_71_proportion, $selection_72_proportion, $selection_73_proportion, $selection_74_proportion, $selection_75_proportion, $selection_76_proportion, $selection_77_proportion, $selection_78_proportion, $selection_79_proportion, $selection_80_proportion, $selection_81_proportion, $selection_82_proportion, $selection_83_proportion, $selection_84_proportion, $selection_85_proportion, $selection_86_proportion, $selection_87_proportion, $selection_88_proportion, $selection_89_proportion, $selection_90_proportion, $selection_total_proportion) = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my ($CL, @control_line, $control_word, $SL, @selection_line, $selection_word);
my $onlys_check = 0;

print "Enter an unselected baseline control file (\"combed_wordcount_seqcount_baseline.txt\"): ";

my $first_entry = <STDIN>;
chomp $first_entry;
my $control = FileHandle->new("$first_entry") or die "ERROR: $!";

while (!$control->eof()) {
	$CL = $control->getline();
	@control_line = split/\t/,$CL;
	$control_word = shift @control_line;
	&control_summer;
}

$control->close();

print "How many sequence reads were there in the control library? ";

my $control_sequence_reads = <STDIN>;
chomp $control_sequence_reads;

print "Enter a selection sequence file (\"combed_wordcount_seqcount_exp.txt\"): ";

my $second_entry = <STDIN>;
chomp $second_entry;
my $selection = FileHandle->new("$second_entry") or die "ERROR: $!";

while (!$selection->eof()) {
	$SL = $selection->getline();
	@selection_line = split/\t/,$SL;
	$selection_word = shift @selection_line;
	&selection_summer;
}

$selection->close();

print "How many sequence reads were there in the selected library? ";

my $selected_sequence_reads = <STDIN>;
chomp $selected_sequence_reads;

$control = FileHandle->new("$first_entry") or die "ERROR: $!";
$selection = FileHandle->new("$second_entry") or die "ERROR: $!";

my $onlys = FileHandle->new(">only_$second_entry") or die "ERROR: $!";
my $selection_proportions = FileHandle->new(">selection_proportions_$second_entry") or die "ERROR: $!";
my $control_proportions = FileHandle->new(">control_proportions_$second_entry") or die "ERROR: $!";
my $selection_occurance = FileHandle->new(">selection_occurance_$second_entry") or die "ERROR: $!";
my $control_occurance = FileHandle->new(">control_occurance_$second_entry") or die "ERROR: $!";

while (!$control->eof() || !$selection->eof()) {
	if ($control->eof() || $selection->eof()) {
		&runout;
	} else {
		$CL = $control->getline();
		@control_line = split/\t/,$CL;
		$control_word = shift @control_line;
		chomp $control_word;
		$SL = $selection->getline();
		@selection_line = split/\t/,$SL;
		$selection_word = shift @selection_line;
		chomp $selection_word;
		&compare_and_report;
	}
}

$control->close();
$selection->close();
$onlys->close();

if ($onlys_check == 0) {
	unlink ("only_$second_entry");
}

sub compare_and_report {
	if ($control_word eq $selection_word) {
		&splitsville;
	} elsif ($control_word lt $selection_word) {
		unless ($control->eof() || $selection->eof()){
			until ($control_word eq $selection_word || $control->eof()) {
				$onlys->print("baseline:\n$CL\n");
				$onlys_check = 1;
				$CL = $control->getline();
				@control_line = split/\t/,$CL;
				$control_word = shift @control_line;
				chomp $control_word;
			}
			&splitsville;
		} else {
			&runout;
		}
	} elsif ($selection_word lt $control_word) {
		unless ($control->eof() || $selection->eof()){
			until ($selection_word eq $control_word || $selection->eof()) {
				$onlys->print("cyclized:\n$SL\n");
				$onlys_check = 1;
				$SL = $selection->getline();
				@selection_line = split/\t/,$SL;
				$selection_word = shift @selection_line;
				chomp $selection_word;
			}
			&splitsville;
		} else {
			&runout;
		}
	}
}

sub runout {
	if ($control->eof()) {
		until ($selection->eof()){
			$SL = $selection->getline();
			$onlys->print("cyclized:\n$SL\n");
			$onlys_check = 1;
		}
	} elsif ($selection->eof()) {
		until ($control->eof()){
			$CL = $control->getline();
			$onlys->print("baseline:\n$CL\n");
			$onlys_check = 1;
		}
	}
}

sub splitsville {
	my $control_hold = shift @control_line;
	chomp $control_hold;
	$control_1_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_2_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_3_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_4_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_5_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_6_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_7_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_8_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_9_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_10_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_11_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_12_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_13_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_14_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_15_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_16_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_17_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_18_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_19_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_20_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_21_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_22_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_23_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_24_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_25_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_26_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_27_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_28_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_29_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_30_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_31_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_32_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_33_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_34_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_35_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_36_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_37_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_38_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_39_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_40_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_41_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_42_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_43_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_44_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_45_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_46_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_47_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_48_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_49_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_50_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_51_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_52_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_53_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_54_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_55_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_56_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_57_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_58_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_59_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_60_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_61_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_62_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_63_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_64_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_65_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_66_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_67_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_68_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_69_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_70_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_71_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_72_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_73_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_74_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_75_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_76_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_77_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_78_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_79_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_80_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_81_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_82_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_83_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_84_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_85_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_86_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_87_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_88_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_89_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_90_position = $control_hold;
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_total_position = $control_hold;
	my $selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_1_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_2_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_3_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_4_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_5_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_6_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_7_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_8_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_9_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_10_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_11_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_12_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_13_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_14_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_15_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_16_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_17_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_18_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_19_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_20_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_21_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_22_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_23_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_24_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_25_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_26_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_27_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_28_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_29_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_30_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_31_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_32_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_33_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_34_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_35_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_36_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_37_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_38_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_39_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_40_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_41_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_42_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_43_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_44_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_45_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_46_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_47_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_48_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_49_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_50_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_51_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_52_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_53_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_54_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_55_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_56_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_57_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_58_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_59_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_60_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_61_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_62_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_63_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_64_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_65_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_66_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_67_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_68_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_69_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_70_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_71_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_72_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_73_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_74_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_75_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_76_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_77_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_78_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_79_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_80_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_81_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_82_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_83_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_84_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_85_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_86_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_87_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_88_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_89_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_90_position = $selection_hold;
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_total_position = $selection_hold;
	unless ($control_1_sum == 0) {
		$control_1_proportion = $control_1_position / $control_1_sum;
	} else {
		$control_1_proportion = "$control_1_position/$control_1_sum";
	}
	unless ($control_2_sum == 0) {
		$control_2_proportion = $control_2_position / $control_2_sum;
	} else {
		$control_2_proportion = "$control_2_position/$control_2_sum";
	}
	unless ($control_3_sum == 0) {
		$control_3_proportion = $control_3_position / $control_3_sum;
	} else {
		$control_3_proportion = "$control_3_position/$control_3_sum";
	}
	unless ($control_4_sum == 0) {
		$control_4_proportion = $control_4_position / $control_4_sum;
	} else {
		$control_4_proportion = "$control_4_position/$control_4_sum";
	}
	unless ($control_5_sum == 0) {
		$control_5_proportion = $control_5_position / $control_5_sum;
	} else {
		$control_5_proportion = "$control_5_position/$control_5_sum";
	}
	unless ($control_6_sum == 0) {
		$control_6_proportion = $control_6_position / $control_6_sum;
	} else {
		$control_6_proportion = "$control_6_position/$control_6_sum";
	}
	unless ($control_7_sum == 0) {
		$control_7_proportion = $control_7_position / $control_7_sum;
	} else {
		$control_7_proportion = "$control_7_position/$control_7_sum";
	}
	unless ($control_8_sum == 0) {
		$control_8_proportion = $control_8_position / $control_8_sum;
	} else {
		$control_8_proportion = "$control_8_position/$control_8_sum";
	}
	unless ($control_9_sum == 0) {
		$control_9_proportion = $control_9_position / $control_9_sum;
	} else {
		$control_9_proportion = "$control_9_position/$control_9_sum";
	}
	unless ($control_10_sum == 0) {
		$control_10_proportion = $control_10_position / $control_10_sum;
	} else {
		$control_10_proportion = "$control_10_position/$control_10_sum";
	}
	unless ($control_11_sum == 0) {
		$control_11_proportion = $control_11_position / $control_11_sum;
	} else {
		$control_11_proportion = "$control_11_position/$control_11_sum";
	}
	unless ($control_12_sum == 0) {
		$control_12_proportion = $control_12_position / $control_12_sum;
	} else {
		$control_12_proportion = "$control_12_position/$control_12_sum";
	}
	unless ($control_13_sum == 0) {
		$control_13_proportion = $control_13_position / $control_13_sum;
	} else {
		$control_13_proportion = "$control_13_position/$control_13_sum";
	}
	unless ($control_14_sum == 0) {
		$control_14_proportion = $control_14_position / $control_14_sum;
	} else {
		$control_14_proportion = "$control_14_position/$control_14_sum";
	}
	unless ($control_15_sum == 0) {
		$control_15_proportion = $control_15_position / $control_15_sum;
	} else {
		$control_15_proportion = "$control_15_position/$control_15_sum";
	}
	unless ($control_16_sum == 0) {
		$control_16_proportion = $control_16_position / $control_16_sum;
	} else {
		$control_16_proportion = "$control_16_position/$control_16_sum";
	}
	unless ($control_17_sum == 0) {
		$control_17_proportion = $control_17_position / $control_17_sum;
	} else {
		$control_17_proportion = "$control_17_position/$control_17_sum";
	}
	unless ($control_18_sum == 0) {
		$control_18_proportion = $control_18_position / $control_18_sum;
	} else {
		$control_18_proportion = "$control_18_position/$control_18_sum";
	}
	unless ($control_19_sum == 0) {
		$control_19_proportion = $control_19_position / $control_19_sum;
	} else {
		$control_19_proportion = "$control_19_position/$control_19_sum";
	}
	unless ($control_20_sum == 0) {
		$control_20_proportion = $control_20_position / $control_20_sum;
	} else {
		$control_20_proportion = "$control_20_position/$control_20_sum";
	}
	unless ($control_21_sum == 0) {
		$control_21_proportion = $control_21_position / $control_21_sum;
	} else {
		$control_21_proportion = "$control_21_position/$control_21_sum";
	}
	unless ($control_22_sum == 0) {
		$control_22_proportion = $control_22_position / $control_22_sum;
	} else {
		$control_22_proportion = "$control_22_position/$control_22_sum";
	}
	unless ($control_23_sum == 0) {
		$control_23_proportion = $control_23_position / $control_23_sum;
	} else {
		$control_23_proportion = "$control_23_position/$control_23_sum";
	}
	unless ($control_24_sum == 0) {
		$control_24_proportion = $control_24_position / $control_24_sum;
	} else {
		$control_24_proportion = "$control_24_position/$control_24_sum";
	}
	unless ($control_25_sum == 0) {
		$control_25_proportion = $control_25_position / $control_25_sum;
	} else {
		$control_25_proportion = "$control_25_position/$control_25_sum";
	}
	unless ($control_26_sum == 0) {
		$control_26_proportion = $control_26_position / $control_26_sum;
	} else {
		$control_26_proportion = "$control_26_position/$control_26_sum";
	}
	unless ($control_27_sum == 0) {
		$control_27_proportion = $control_27_position / $control_27_sum;
	} else {
		$control_27_proportion = "$control_27_position/$control_27_sum";
	}
	unless ($control_28_sum == 0) {
		$control_28_proportion = $control_28_position / $control_28_sum;
	} else {
		$control_28_proportion = "$control_28_position/$control_28_sum";
	}
	unless ($control_29_sum == 0) {
		$control_29_proportion = $control_29_position / $control_29_sum;
	} else {
		$control_29_proportion = "$control_29_position/$control_29_sum";
	}
	unless ($control_30_sum == 0) {
		$control_30_proportion = $control_30_position / $control_30_sum;
	} else {
		$control_30_proportion = "$control_30_position/$control_30_sum";
	}
	unless ($control_31_sum == 0) {
		$control_31_proportion = $control_31_position / $control_31_sum;
	} else {
		$control_31_proportion = "$control_31_position/$control_31_sum";
	}
	unless ($control_32_sum == 0) {
		$control_32_proportion = $control_32_position / $control_32_sum;
	} else {
		$control_32_proportion = "$control_32_position/$control_32_sum";
	}
	unless ($control_33_sum == 0) {
		$control_33_proportion = $control_33_position / $control_33_sum;
	} else {
		$control_33_proportion = "$control_33_position/$control_33_sum";
	}
	unless ($control_34_sum == 0) {
		$control_34_proportion = $control_34_position / $control_34_sum;
	} else {
		$control_34_proportion = "$control_34_position/$control_34_sum";
	}
	unless ($control_35_sum == 0) {
		$control_35_proportion = $control_35_position / $control_35_sum;
	} else {
		$control_35_proportion = "$control_35_position/$control_35_sum";
	}
	unless ($control_36_sum == 0) {
		$control_36_proportion = $control_36_position / $control_36_sum;
	} else {
		$control_36_proportion = "$control_36_position/$control_36_sum";
	}
	unless ($control_37_sum == 0) {
		$control_37_proportion = $control_37_position / $control_37_sum;
	} else {
		$control_37_proportion = "$control_37_position/$control_37_sum";
	}
	unless ($control_38_sum == 0) {
		$control_38_proportion = $control_38_position / $control_38_sum;
	} else {
		$control_38_proportion = "$control_38_position/$control_38_sum";
	}
	unless ($control_39_sum == 0) {
		$control_39_proportion = $control_39_position / $control_39_sum;
	} else {
		$control_39_proportion = "$control_39_position/$control_39_sum";
	}
	unless ($control_40_sum == 0) {
		$control_40_proportion = $control_40_position / $control_40_sum;
	} else {
		$control_40_proportion = "$control_40_position/$control_40_sum";
	}
	unless ($control_41_sum == 0) {
		$control_41_proportion = $control_41_position / $control_41_sum;
	} else {
		$control_41_proportion = "$control_41_position/$control_41_sum";
	}
	unless ($control_42_sum == 0) {
		$control_42_proportion = $control_42_position / $control_42_sum;
	} else {
		$control_42_proportion = "$control_42_position/$control_42_sum";
	}
	unless ($control_43_sum == 0) {
		$control_43_proportion = $control_43_position / $control_43_sum;
	} else {
		$control_43_proportion = "$control_43_position/$control_43_sum";
	}
	unless ($control_44_sum == 0) {
		$control_44_proportion = $control_44_position / $control_44_sum;
	} else {
		$control_44_proportion = "$control_44_position/$control_44_sum";
	}
	unless ($control_45_sum == 0) {
		$control_45_proportion = $control_45_position / $control_45_sum;
	} else {
		$control_45_proportion = "$control_45_position/$control_45_sum";
	}
	unless ($control_46_sum == 0) {
		$control_46_proportion = $control_46_position / $control_46_sum;
	} else {
		$control_46_proportion = "$control_46_position/$control_46_sum";
	}
	unless ($control_47_sum == 0) {
		$control_47_proportion = $control_47_position / $control_47_sum;
	} else {
		$control_47_proportion = "$control_47_position/$control_47_sum";
	}
	unless ($control_48_sum == 0) {
		$control_48_proportion = $control_48_position / $control_48_sum;
	} else {
		$control_48_proportion = "$control_48_position/$control_48_sum";
	}
	unless ($control_49_sum == 0) {
		$control_49_proportion = $control_49_position / $control_49_sum;
	} else {
		$control_49_proportion = "$control_49_position/$control_49_sum";
	}
	unless ($control_50_sum == 0) {
		$control_50_proportion = $control_50_position / $control_50_sum;
	} else {
		$control_50_proportion = "$control_50_position/$control_50_sum";
	}
	unless ($control_51_sum == 0) {
		$control_51_proportion = $control_51_position / $control_51_sum;
	} else {
		$control_51_proportion = "$control_51_position/$control_51_sum";
	}
	unless ($control_52_sum == 0) {
		$control_52_proportion = $control_52_position / $control_52_sum;
	} else {
		$control_52_proportion = "$control_52_position/$control_52_sum";
	}
	unless ($control_53_sum == 0) {
		$control_53_proportion = $control_53_position / $control_53_sum;
	} else {
		$control_53_proportion = "$control_53_position/$control_53_sum";
	}
	unless ($control_54_sum == 0) {
		$control_54_proportion = $control_54_position / $control_54_sum;
	} else {
		$control_54_proportion = "$control_54_position/$control_54_sum";
	}
	unless ($control_55_sum == 0) {
		$control_55_proportion = $control_55_position / $control_55_sum;
	} else {
		$control_55_proportion = "$control_55_position/$control_55_sum";
	}
	unless ($control_56_sum == 0) {
		$control_56_proportion = $control_56_position / $control_56_sum;
	} else {
		$control_56_proportion = "$control_56_position/$control_56_sum";
	}
	unless ($control_57_sum == 0) {
		$control_57_proportion = $control_57_position / $control_57_sum;
	} else {
		$control_57_proportion = "$control_57_position/$control_57_sum";
	}
	unless ($control_58_sum == 0) {
		$control_58_proportion = $control_58_position / $control_58_sum;
	} else {
		$control_58_proportion = "$control_58_position/$control_58_sum";
	}
	unless ($control_59_sum == 0) {
		$control_59_proportion = $control_59_position / $control_59_sum;
	} else {
		$control_59_proportion = "$control_59_position/$control_59_sum";
	}
	unless ($control_60_sum == 0) {
		$control_60_proportion = $control_60_position / $control_60_sum;
	} else {
		$control_60_proportion = "$control_60_position/$control_60_sum";
	}
	unless ($control_61_sum == 0) {
		$control_61_proportion = $control_61_position / $control_61_sum;
	} else {
		$control_61_proportion = "$control_61_position/$control_61_sum";
	}
	unless ($control_62_sum == 0) {
		$control_62_proportion = $control_62_position / $control_62_sum;
	} else {
		$control_62_proportion = "$control_62_position/$control_62_sum";
	}
	unless ($control_63_sum == 0) {
		$control_63_proportion = $control_63_position / $control_63_sum;
	} else {
		$control_63_proportion = "$control_63_position/$control_63_sum";
	}
	unless ($control_64_sum == 0) {
		$control_64_proportion = $control_64_position / $control_64_sum;
	} else {
		$control_64_proportion = "$control_64_position/$control_64_sum";
	}
	unless ($control_65_sum == 0) {
		$control_65_proportion = $control_65_position / $control_65_sum;
	} else {
		$control_65_proportion = "$control_65_position/$control_65_sum";
	}
	unless ($control_66_sum == 0) {
		$control_66_proportion = $control_66_position / $control_66_sum;
	} else {
		$control_66_proportion = "$control_66_position/$control_66_sum";
	}
	unless ($control_67_sum == 0) {
		$control_67_proportion = $control_67_position / $control_67_sum;
	} else {
		$control_67_proportion = "$control_67_position/$control_67_sum";
	}
	unless ($control_68_sum == 0) {
		$control_68_proportion = $control_68_position / $control_68_sum;
	} else {
		$control_68_proportion = "$control_68_position/$control_68_sum";
	}
	unless ($control_69_sum == 0) {
		$control_69_proportion = $control_69_position / $control_69_sum;
	} else {
		$control_69_proportion = "$control_69_position/$control_69_sum";
	}
	unless ($control_70_sum == 0) {
		$control_70_proportion = $control_70_position / $control_70_sum;
	} else {
		$control_70_proportion = "$control_70_position/$control_70_sum";
	}
	unless ($control_71_sum == 0) {
		$control_71_proportion = $control_71_position / $control_71_sum;
	} else {
		$control_71_proportion = "$control_71_position/$control_71_sum";
	}
	unless ($control_72_sum == 0) {
		$control_72_proportion = $control_72_position / $control_72_sum;
	} else {
		$control_72_proportion = "$control_72_position/$control_72_sum";
	}
	unless ($control_73_sum == 0) {
		$control_73_proportion = $control_73_position / $control_73_sum;
	} else {
		$control_73_proportion = "$control_73_position/$control_73_sum";
	}
	unless ($control_74_sum == 0) {
		$control_74_proportion = $control_74_position / $control_74_sum;
	} else {
		$control_74_proportion = "$control_74_position/$control_74_sum";
	}
	unless ($control_75_sum == 0) {
		$control_75_proportion = $control_75_position / $control_75_sum;
	} else {
		$control_75_proportion = "$control_75_position/$control_75_sum";
	}
	unless ($control_76_sum == 0) {
		$control_76_proportion = $control_76_position / $control_76_sum;
	} else {
		$control_76_proportion = "$control_76_position/$control_76_sum";
	}
	unless ($control_77_sum == 0) {
		$control_77_proportion = $control_77_position / $control_77_sum;
	} else {
		$control_77_proportion = "$control_77_position/$control_77_sum";
	}
	unless ($control_78_sum == 0) {
		$control_78_proportion = $control_78_position / $control_78_sum;
	} else {
		$control_78_proportion = "$control_78_position/$control_78_sum";
	}
	unless ($control_79_sum == 0) {
		$control_79_proportion = $control_79_position / $control_79_sum;
	} else {
		$control_79_proportion = "$control_79_position/$control_79_sum";
	}
	unless ($control_80_sum == 0) {
		$control_80_proportion = $control_80_position / $control_80_sum;
	} else {
		$control_80_proportion = "$control_80_position/$control_80_sum";
	}
	unless ($control_81_sum == 0) {
		$control_81_proportion = $control_81_position / $control_81_sum;
	} else {
		$control_81_proportion = "$control_81_position/$control_81_sum";
	}
	unless (length($control_word) >= 10) {
		unless ($control_82_sum == 0) {
			$control_82_proportion = $control_82_position / $control_82_sum;
		} else {
			$control_82_proportion = "$control_82_position/$control_82_sum";
		}
	}
	unless (length($control_word) >= 9) {
		unless ($control_83_sum == 0) {
			$control_83_proportion = $control_83_position / $control_83_sum;
		} else {
			$control_83_proportion = "$control_83_position/$control_83_sum";
		}
	}
	unless (length($control_word) >= 8) {
		unless ($control_84_sum == 0) {
			$control_84_proportion = $control_84_position / $control_84_sum;
		} else {
			$control_84_proportion = "$control_84_position/$control_84_sum";
		}
	}
	unless (length($control_word) >= 7) {
		unless ($control_85_sum == 0) {
			$control_85_proportion = $control_85_position / $control_85_sum;
		} else {
			$control_85_proportion = "$control_85_position/$control_85_sum";
		}
	}
	unless (length($control_word) >= 6) {
		unless ($control_86_sum == 0) {
			$control_86_proportion = $control_86_position / $control_86_sum;
		} else {
			$control_86_proportion = "$control_86_position/$control_86_sum";
		}
	}
	unless (length($control_word) >= 5) {
		unless ($control_87_sum == 0) {
			$control_87_proportion = $control_87_position / $control_87_sum;
		} else {
			$control_87_proportion = "$control_87_position/$control_87_sum";
		}
	}
	unless (length($control_word) >= 4) {
		unless ($control_88_sum == 0) {
			$control_88_proportion = $control_88_position / $control_88_sum;
		} else {
			$control_88_proportion = "$control_88_position/$control_88_sum";
		}
	}
	unless (length($control_word) >= 3) {
		unless ($control_89_sum == 0) {
			$control_89_proportion = $control_89_position / $control_89_sum;
		} else {
			$control_89_proportion = "$control_89_position/$control_89_sum";
		}
	}	
	unless (length($control_word) >= 2) {
		unless ($control_90_sum == 0) {
			$control_90_proportion = $control_90_position / $control_90_sum;
		} else {
			$control_90_proportion = "$control_90_position/$control_90_sum";
		}
	}
	$control_total_proportion = $control_total_position / $control_total_sum;
	unless ($selection_1_sum == 0) {
		$selection_1_proportion = $selection_1_position / $selection_1_sum;
	} else {
		$selection_1_proportion = "$selection_1_position/$selection_1_sum";
	}
	unless ($selection_2_sum == 0) {
		$selection_2_proportion = $selection_2_position / $selection_2_sum;
	} else {
		$selection_2_proportion = "$selection_2_position/$selection_2_sum";
	}
	unless ($selection_3_sum == 0) {
		$selection_3_proportion = $selection_3_position / $selection_3_sum;
	} else {
		$selection_3_proportion = "$selection_3_position/$selection_3_sum";
	}
	unless ($selection_4_sum == 0) {
		$selection_4_proportion = $selection_4_position / $selection_4_sum;
	} else {
		$selection_4_proportion = "$selection_4_position/$selection_4_sum";
	}
	unless ($selection_5_sum == 0) {
		$selection_5_proportion = $selection_5_position / $selection_5_sum;
	} else {
		$selection_5_proportion = "$selection_5_position/$selection_5_sum";
	}
	unless ($selection_6_sum == 0) {
		$selection_6_proportion = $selection_6_position / $selection_6_sum;
	} else {
		$selection_6_proportion = "$selection_6_position/$selection_6_sum";
	}
	unless ($selection_7_sum == 0) {
		$selection_7_proportion = $selection_7_position / $selection_7_sum;
	} else {
		$selection_7_proportion = "$selection_7_position/$selection_7_sum";
	}
	unless ($selection_8_sum == 0) {
		$selection_8_proportion = $selection_8_position / $selection_8_sum;
	} else {
		$selection_8_proportion = "$selection_8_position/$selection_8_sum";
	}
	unless ($selection_9_sum == 0) {
		$selection_9_proportion = $selection_9_position / $selection_9_sum;
	} else {
		$selection_9_proportion = "$selection_9_position/$selection_9_sum";
	}
	unless ($selection_10_sum == 0) {
		$selection_10_proportion = $selection_10_position / $selection_10_sum;
	} else {
		$selection_10_proportion = "$selection_10_position/$selection_10_sum";
	}
	unless ($selection_11_sum == 0) {
		$selection_11_proportion = $selection_11_position / $selection_11_sum;
	} else {
		$selection_11_proportion = "$selection_11_position/$selection_11_sum";
	}
	unless ($selection_12_sum == 0) {
		$selection_12_proportion = $selection_12_position / $selection_12_sum;
	} else {
		$selection_12_proportion = "$selection_12_position/$selection_12_sum";
	}
	unless ($selection_13_sum == 0) {
		$selection_13_proportion = $selection_13_position / $selection_13_sum;
	} else {
		$selection_13_proportion = "$selection_13_position/$selection_13_sum";
	}
	unless ($selection_14_sum == 0) {
		$selection_14_proportion = $selection_14_position / $selection_14_sum;
	} else {
		$selection_14_proportion = "$selection_14_position/$selection_14_sum";
	}
	unless ($selection_15_sum == 0) {
		$selection_15_proportion = $selection_15_position / $selection_15_sum;
	} else {
		$selection_15_proportion = "$selection_15_position/$selection_15_sum";
	}
	unless ($selection_16_sum == 0) {
		$selection_16_proportion = $selection_16_position / $selection_16_sum;
	} else {
		$selection_16_proportion = "$selection_16_position/$selection_16_sum";
	}
	unless ($selection_17_sum == 0) {
		$selection_17_proportion = $selection_17_position / $selection_17_sum;
	} else {
		$selection_17_proportion = "$selection_17_position/$selection_17_sum";
	}
	unless ($selection_18_sum == 0) {
		$selection_18_proportion = $selection_18_position / $selection_18_sum;
	} else {
		$selection_18_proportion = "$selection_18_position/$selection_18_sum";
	}
	unless ($selection_19_sum == 0) {
		$selection_19_proportion = $selection_19_position / $selection_19_sum;
	} else {
		$selection_19_proportion = "$selection_19_position/$selection_19_sum";
	}
	unless ($selection_20_sum == 0) {
		$selection_20_proportion = $selection_20_position / $selection_20_sum;
	} else {
		$selection_20_proportion = "$selection_20_position/$selection_20_sum";
	}
	unless ($selection_21_sum == 0) {
		$selection_21_proportion = $selection_21_position / $selection_21_sum;
	} else {
		$selection_21_proportion = "$selection_21_position/$selection_21_sum";
	}
	unless ($selection_22_sum == 0) {
		$selection_22_proportion = $selection_22_position / $selection_22_sum;
	} else {
		$selection_22_proportion = "$selection_22_position/$selection_22_sum";
	}
	unless ($selection_23_sum == 0) {
		$selection_23_proportion = $selection_23_position / $selection_23_sum;
	} else {
		$selection_23_proportion = "$selection_23_position/$selection_23_sum";
	}
	unless ($selection_24_sum == 0) {
		$selection_24_proportion = $selection_24_position / $selection_24_sum;
	} else {
		$selection_24_proportion = "$selection_24_position/$selection_24_sum";
	}
	unless ($selection_25_sum == 0) {
		$selection_25_proportion = $selection_25_position / $selection_25_sum;
	} else {
		$selection_25_proportion = "$selection_25_position/$selection_25_sum";
	}
	unless ($selection_26_sum == 0) {
		$selection_26_proportion = $selection_26_position / $selection_26_sum;
	} else {
		$selection_26_proportion = "$selection_26_position/$selection_26_sum";
	}
	unless ($selection_27_sum == 0) {
		$selection_27_proportion = $selection_27_position / $selection_27_sum;
	} else {
		$selection_27_proportion = "$selection_27_position/$selection_27_sum";
	}
	unless ($selection_28_sum == 0) {
		$selection_28_proportion = $selection_28_position / $selection_28_sum;
	} else {
		$selection_28_proportion = "$selection_28_position/$selection_28_sum";
	}
	unless ($selection_29_sum == 0) {
		$selection_29_proportion = $selection_29_position / $selection_29_sum;
	} else {
		$selection_29_proportion = "$selection_29_position/$selection_29_sum";
	}
	unless ($selection_30_sum == 0) {
		$selection_30_proportion = $selection_30_position / $selection_30_sum;
	} else {
		$selection_30_proportion = "$selection_30_position/$selection_30_sum";
	}
	unless ($selection_31_sum == 0) {
		$selection_31_proportion = $selection_31_position / $selection_31_sum;
	} else {
		$selection_31_proportion = "$selection_31_position/$selection_31_sum";
	}
	unless ($selection_32_sum == 0) {
		$selection_32_proportion = $selection_32_position / $selection_32_sum;
	} else {
		$selection_32_proportion = "$selection_32_position/$selection_32_sum";
	}
	unless ($selection_33_sum == 0) {
		$selection_33_proportion = $selection_33_position / $selection_33_sum;
	} else {
		$selection_33_proportion = "$selection_33_position/$selection_33_sum";
	}
	unless ($selection_34_sum == 0) {
		$selection_34_proportion = $selection_34_position / $selection_34_sum;
	} else {
		$selection_34_proportion = "$selection_34_position/$selection_34_sum";
	}
	unless ($selection_35_sum == 0) {
		$selection_35_proportion = $selection_35_position / $selection_35_sum;
	} else {
		$selection_35_proportion = "$selection_35_position/$selection_35_sum";
	}
	unless ($selection_36_sum == 0) {
		$selection_36_proportion = $selection_36_position / $selection_36_sum;
	} else {
		$selection_36_proportion = "$selection_36_position/$selection_36_sum";
	}
	unless ($selection_37_sum == 0) {
		$selection_37_proportion = $selection_37_position / $selection_37_sum;
	} else {
		$selection_37_proportion = "$selection_37_position/$selection_37_sum";
	}
	unless ($selection_38_sum == 0) {
		$selection_38_proportion = $selection_38_position / $selection_38_sum;
	} else {
		$selection_38_proportion = "$selection_38_position/$selection_38_sum";
	}
	unless ($selection_39_sum == 0) {
		$selection_39_proportion = $selection_39_position / $selection_39_sum;
	} else {
		$selection_39_proportion = "$selection_39_position/$selection_39_sum";
	}
	unless ($selection_40_sum == 0) {
		$selection_40_proportion = $selection_40_position / $selection_40_sum;
	} else {
		$selection_40_proportion = "$selection_40_position/$selection_40_sum";
	}
	unless ($selection_41_sum == 0) {
		$selection_41_proportion = $selection_41_position / $selection_41_sum;
	} else {
		$selection_41_proportion = "$selection_41_position/$selection_41_sum";
	}
	unless ($selection_42_sum == 0) {
		$selection_42_proportion = $selection_42_position / $selection_42_sum;
	} else {
		$selection_42_proportion = "$selection_42_position/$selection_42_sum";
	}
	unless ($selection_43_sum == 0) {
		$selection_43_proportion = $selection_43_position / $selection_43_sum;
	} else {
		$selection_43_proportion = "$selection_43_position/$selection_43_sum";
	}
	unless ($selection_44_sum == 0) {
		$selection_44_proportion = $selection_44_position / $selection_44_sum;
	} else {
		$selection_44_proportion = "$selection_44_position/$selection_44_sum";
	}
	unless ($selection_45_sum == 0) {
		$selection_45_proportion = $selection_45_position / $selection_45_sum;
	} else {
		$selection_45_proportion = "$selection_45_position/$selection_45_sum";
	}
	unless ($selection_46_sum == 0) {
		$selection_46_proportion = $selection_46_position / $selection_46_sum;
	} else {
		$selection_46_proportion = "$selection_46_position/$selection_46_sum";
	}
	unless ($selection_47_sum == 0) {
		$selection_47_proportion = $selection_47_position / $selection_47_sum;
	} else {
		$selection_47_proportion = "$selection_47_position/$selection_47_sum";
	}
	unless ($selection_48_sum == 0) {
		$selection_48_proportion = $selection_48_position / $selection_48_sum;
	} else {
		$selection_48_proportion = "$selection_48_position/$selection_48_sum";
	}
	unless ($selection_49_sum == 0) {
		$selection_49_proportion = $selection_49_position / $selection_49_sum;
	} else {
		$selection_49_proportion = "$selection_49_position/$selection_49_sum";
	}
	unless ($selection_50_sum == 0) {
		$selection_50_proportion = $selection_50_position / $selection_50_sum;
	} else {
		$selection_50_proportion = "$selection_50_position/$selection_50_sum";
	}
	unless ($selection_51_sum == 0) {
		$selection_51_proportion = $selection_51_position / $selection_51_sum;
	} else {
		$selection_51_proportion = "$selection_51_position/$selection_51_sum";
	}
	unless ($selection_52_sum == 0) {
		$selection_52_proportion = $selection_52_position / $selection_52_sum;
	} else {
		$selection_52_proportion = "$selection_52_position/$selection_52_sum";
	}
	unless ($selection_53_sum == 0) {
		$selection_53_proportion = $selection_53_position / $selection_53_sum;
	} else {
		$selection_53_proportion = "$selection_53_position/$selection_53_sum";
	}
	unless ($selection_54_sum == 0) {
		$selection_54_proportion = $selection_54_position / $selection_54_sum;
	} else {
		$selection_54_proportion = "$selection_54_position/$selection_54_sum";
	}
	unless ($selection_55_sum == 0) {
		$selection_55_proportion = $selection_55_position / $selection_55_sum;
	} else {
		$selection_55_proportion = "$selection_55_position/$selection_55_sum";
	}
	unless ($selection_56_sum == 0) {
		$selection_56_proportion = $selection_56_position / $selection_56_sum;
	} else {
		$selection_56_proportion = "$selection_56_position/$selection_56_sum";
	}
	unless ($selection_57_sum == 0) {
		$selection_57_proportion = $selection_57_position / $selection_57_sum;
	} else {
		$selection_57_proportion = "$selection_57_position/$selection_57_sum";
	}
	unless ($selection_58_sum == 0) {
		$selection_58_proportion = $selection_58_position / $selection_58_sum;
	} else {
		$selection_58_proportion = "$selection_58_position/$selection_58_sum";
	}
	unless ($selection_59_sum == 0) {
		$selection_59_proportion = $selection_59_position / $selection_59_sum;
	} else {
		$selection_59_proportion = "$selection_59_position/$selection_59_sum";
	}
	unless ($selection_60_sum == 0) {
		$selection_60_proportion = $selection_60_position / $selection_60_sum;
	} else {
		$selection_60_proportion = "$selection_60_position/$selection_60_sum";
	}
	unless ($selection_61_sum == 0) {
		$selection_61_proportion = $selection_61_position / $selection_61_sum;
	} else {
		$selection_61_proportion = "$selection_61_position/$selection_61_sum";
	}
	unless ($selection_62_sum == 0) {
		$selection_62_proportion = $selection_62_position / $selection_62_sum;
	} else {
		$selection_62_proportion = "$selection_62_position/$selection_62_sum";
	}
	unless ($selection_63_sum == 0) {
		$selection_63_proportion = $selection_63_position / $selection_63_sum;
	} else {
		$selection_63_proportion = "$selection_63_position/$selection_63_sum";
	}
	unless ($selection_64_sum == 0) {
		$selection_64_proportion = $selection_64_position / $selection_64_sum;
	} else {
		$selection_64_proportion = "$selection_64_position/$selection_64_sum";
	}
	unless ($selection_65_sum == 0) {
		$selection_65_proportion = $selection_65_position / $selection_65_sum;
	} else {
		$selection_65_proportion = "$selection_65_position/$selection_65_sum";
	}
	unless ($selection_66_sum == 0) {
		$selection_66_proportion = $selection_66_position / $selection_66_sum;
	} else {
		$selection_66_proportion = "$selection_66_position/$selection_66_sum";
	}
	unless ($selection_67_sum == 0) {
		$selection_67_proportion = $selection_67_position / $selection_67_sum;
	} else {
		$selection_67_proportion = "$selection_67_position/$selection_67_sum";
	}
	unless ($selection_68_sum == 0) {
		$selection_68_proportion = $selection_68_position / $selection_68_sum;
	} else {
		$selection_68_proportion = "$selection_68_position/$selection_68_sum";
	}
	unless ($selection_69_sum == 0) {
		$selection_69_proportion = $selection_69_position / $selection_69_sum;
	} else {
		$selection_69_proportion = "$selection_69_position/$selection_69_sum";
	}
	unless ($selection_70_sum == 0) {
		$selection_70_proportion = $selection_70_position / $selection_70_sum;
	} else {
		$selection_70_proportion = "$selection_70_position/$selection_70_sum";
	}
	unless ($selection_71_sum == 0) {
		$selection_71_proportion = $selection_71_position / $selection_71_sum;
	} else {
		$selection_71_proportion = "$selection_71_position/$selection_71_sum";
	}
	unless ($selection_72_sum == 0) {
		$selection_72_proportion = $selection_72_position / $selection_72_sum;
	} else {
		$selection_72_proportion = "$selection_72_position/$selection_72_sum";
	}
	unless ($selection_73_sum == 0) {
		$selection_73_proportion = $selection_73_position / $selection_73_sum;
	} else {
		$selection_73_proportion = "$selection_73_position/$selection_73_sum";
	}
	unless ($selection_74_sum == 0) {
		$selection_74_proportion = $selection_74_position / $selection_74_sum;
	} else {
		$selection_74_proportion = "$selection_74_position/$selection_74_sum";
	}
	unless ($selection_75_sum == 0) {
		$selection_75_proportion = $selection_75_position / $selection_75_sum;
	} else {
		$selection_75_proportion = "$selection_75_position/$selection_75_sum";
	}
	unless ($selection_76_sum == 0) {
		$selection_76_proportion = $selection_76_position / $selection_76_sum;
	} else {
		$selection_76_proportion = "$selection_76_position/$selection_76_sum";
	}
	unless ($selection_77_sum == 0) {
		$selection_77_proportion = $selection_77_position / $selection_77_sum;
	} else {
		$selection_77_proportion = "$selection_77_position/$selection_77_sum";
	}
	unless ($selection_78_sum == 0) {
		$selection_78_proportion = $selection_78_position / $selection_78_sum;
	} else {
		$selection_78_proportion = "$selection_78_position/$selection_78_sum";
	}
	unless ($selection_79_sum == 0) {
		$selection_79_proportion = $selection_79_position / $selection_79_sum;
	} else {
		$selection_79_proportion = "$selection_79_position/$selection_79_sum";
	}
	unless ($selection_80_sum == 0) {
		$selection_80_proportion = $selection_80_position / $selection_80_sum;
	} else {
		$selection_80_proportion = "$selection_80_position/$selection_80_sum";
	}
	unless ($selection_81_sum == 0) {
		$selection_81_proportion = $selection_81_position / $selection_81_sum;
	} else {
		$selection_81_proportion = "$selection_81_position/$selection_81_sum";
	}
	unless (length($selection_word) >= 10) {
		unless ($selection_82_sum == 0) {
			$selection_82_proportion = $selection_82_position / $selection_82_sum;
		} else {
			$selection_82_proportion = "$selection_82_position/$selection_82_sum";
		}
	}
	unless (length($selection_word) >= 9) {
		unless ($selection_83_sum == 0) {
			$selection_83_proportion = $selection_83_position / $selection_83_sum;
		} else {
			$selection_83_proportion = "$selection_83_position/$selection_83_sum";
		}
	}
	unless (length($selection_word) >= 8) {
		unless ($selection_84_sum == 0) {
			$selection_84_proportion = $selection_84_position / $selection_84_sum;
		} else {
			$selection_84_proportion = "$selection_84_position/$selection_84_sum";
		}
	}
	unless (length($selection_word) >= 7) {
		unless ($selection_85_sum == 0) {
			$selection_85_proportion = $selection_85_position / $selection_85_sum;
		} else {
			$selection_85_proportion = "$selection_85_position/$selection_85_sum";
		}
	}
	unless (length($selection_word) >= 6) {
		unless ($selection_86_sum == 0) {
			$selection_86_proportion = $selection_86_position / $selection_86_sum;
		} else {
			$selection_86_proportion = "$selection_86_position/$selection_86_sum";
		}
	}
	unless (length($selection_word) >= 5) {
		unless ($selection_87_sum == 0) {
			$selection_87_proportion = $selection_87_position / $selection_87_sum;
		} else {
			$selection_87_proportion = "$selection_87_position/$selection_87_sum";
		}
	}
	unless (length($selection_word) >= 4) {
		unless ($selection_88_sum == 0) {
			$selection_88_proportion = $selection_88_position / $selection_88_sum;
		} else {
			$selection_88_proportion = "$selection_88_position/$selection_88_sum";
		}
	}
	unless (length($selection_word) >= 3) {
		unless ($selection_89_sum == 0) {
			$selection_89_proportion = $selection_89_position / $selection_89_sum;
		} else {
			$selection_89_proportion = "$selection_89_position/$selection_89_sum";
		}
	}	
	unless (length($selection_word) >= 2) {
		unless ($selection_90_sum == 0) {
			$selection_90_proportion = $selection_90_position / $selection_90_sum;
		} else {
			$selection_90_proportion = "$selection_90_position/$selection_90_sum";
		}
	}
	$selection_total_proportion = $selection_total_position / $selection_total_sum;
	$selection_proportions->print("$selection_word\t$selection_1_proportion\t$selection_2_proportion\t$selection_3_proportion\t$selection_4_proportion\t$selection_5_proportion\t$selection_6_proportion\t$selection_7_proportion\t$selection_8_proportion\t$selection_9_proportion\t$selection_10_proportion\t$selection_11_proportion\t$selection_12_proportion\t$selection_13_proportion\t$selection_14_proportion\t$selection_15_proportion\t$selection_16_proportion\t$selection_17_proportion\t$selection_18_proportion\t$selection_19_proportion\t$selection_20_proportion\t$selection_21_proportion\t$selection_22_proportion\t$selection_23_proportion\t$selection_24_proportion\t$selection_25_proportion\t$selection_26_proportion\t$selection_27_proportion\t$selection_28_proportion\t$selection_29_proportion\t$selection_30_proportion\t$selection_31_proportion\t$selection_32_proportion\t$selection_33_proportion\t$selection_34_proportion\t$selection_35_proportion\t$selection_36_proportion\t$selection_37_proportion\t$selection_38_proportion\t$selection_39_proportion\t$selection_40_proportion\t$selection_41_proportion\t$selection_42_proportion\t$selection_43_proportion\t$selection_44_proportion\t$selection_45_proportion\t$selection_46_proportion\t$selection_47_proportion\t$selection_48_proportion\t$selection_49_proportion\t$selection_50_proportion\t$selection_51_proportion\t$selection_52_proportion\t$selection_53_proportion\t$selection_54_proportion\t$selection_55_proportion\t$selection_56_proportion\t$selection_57_proportion\t$selection_58_proportion\t$selection_59_proportion\t$selection_60_proportion\t$selection_61_proportion\t$selection_62_proportion\t$selection_63_proportion\t$selection_64_proportion\t$selection_65_proportion\t$selection_66_proportion\t$selection_67_proportion\t$selection_68_proportion\t$selection_69_proportion\t$selection_70_proportion\t$selection_71_proportion\t$selection_72_proportion\t$selection_73_proportion\t$selection_74_proportion\t$selection_75_proportion\t$selection_76_proportion\t$selection_77_proportion\t$selection_78_proportion\t$selection_79_proportion\t$selection_80_proportion\t$selection_81_proportion\t");
	$control_proportions->print("$control_word\t$control_1_proportion\t$control_2_proportion\t$control_3_proportion\t$control_4_proportion\t$control_5_proportion\t$control_6_proportion\t$control_7_proportion\t$control_8_proportion\t$control_9_proportion\t$control_10_proportion\t$control_11_proportion\t$control_12_proportion\t$control_13_proportion\t$control_14_proportion\t$control_15_proportion\t$control_16_proportion\t$control_17_proportion\t$control_18_proportion\t$control_19_proportion\t$control_20_proportion\t$control_21_proportion\t$control_22_proportion\t$control_23_proportion\t$control_24_proportion\t$control_25_proportion\t$control_26_proportion\t$control_27_proportion\t$control_28_proportion\t$control_29_proportion\t$control_30_proportion\t$control_31_proportion\t$control_32_proportion\t$control_33_proportion\t$control_34_proportion\t$control_35_proportion\t$control_36_proportion\t$control_37_proportion\t$control_38_proportion\t$control_39_proportion\t$control_40_proportion\t$control_41_proportion\t$control_42_proportion\t$control_43_proportion\t$control_44_proportion\t$control_45_proportion\t$control_46_proportion\t$control_47_proportion\t$control_48_proportion\t$control_49_proportion\t$control_50_proportion\t$control_51_proportion\t$control_52_proportion\t$control_53_proportion\t$control_54_proportion\t$control_55_proportion\t$control_56_proportion\t$control_57_proportion\t$control_58_proportion\t$control_59_proportion\t$control_60_proportion\t$control_61_proportion\t$control_62_proportion\t$control_63_proportion\t$control_64_proportion\t$control_65_proportion\t$control_66_proportion\t$control_67_proportion\t$control_68_proportion\t$control_69_proportion\t$control_70_proportion\t$control_71_proportion\t$control_72_proportion\t$control_73_proportion\t$control_74_proportion\t$control_75_proportion\t$control_76_proportion\t$control_77_proportion\t$control_78_proportion\t$control_79_proportion\t$control_80_proportion\t$control_81_proportion\t");
	if (length($control_word) == 10) {
		$selection_proportions->print("\t\t\t\t\t\t\t\t\t$selection_total_proportion\n");
		$control_proportions->print("\t\t\t\t\t\t\t\t\t$control_total_proportion\n");
	} else {
		$selection_proportions->print("$selection_82_proportion\t");
		$control_proportions->print("$control_82_proportion\t");
		if (length($control_word) == 9) {
			$selection_proportions->print("\t\t\t\t\t\t\t\t$selection_total_proportion\n");
			$control_proportions->print("\t\t\t\t\t\t\t\t$control_total_proportion\n");
		} else {
			$selection_proportions->print("$selection_83_proportion\t");
				$control_proportions->print("$control_83_proportion\t");
			if (length($control_word) == 8) {
				$selection_proportions->print("\t\t\t\t\t\t\t$selection_total_proportion\n");
				$control_proportions->print("\t\t\t\t\t\t\t$control_total_proportion\n");
			} else {
				$selection_proportions->print("$selection_84_proportion\t");
				$control_proportions->print("$control_84_proportion\t");
				if (length($control_word) == 7) {
					$selection_proportions->print("\t\t\t\t\t\t$selection_total_proportion\n");
					$control_proportions->print("\t\t\t\t\t\t$control_total_proportion\n");
				} else {
					$selection_proportions->print("$selection_85_proportion\t");
					$control_proportions->print("$control_85_proportion\t");
					if (length($control_word) == 6) {
						$selection_proportions->print("\t\t\t\t\t$selection_total_proportion\n");
						$control_proportions->print("\t\t\t\t\t$control_total_proportion\n");
					} else {
						$selection_proportions->print("$selection_86_proportion\t");
						$control_proportions->print("$control_86_proportion\t");
						if (length($control_word) == 5) {
							$selection_proportions->print("\t\t\t\t$selection_total_proportion\n");
							$control_proportions->print("\t\t\t\t$control_total_proportion\n");
						} else {
							$selection_proportions->print("$selection_87_proportion\t");
							$control_proportions->print("$control_87_proportion\t");
							if (length($control_word) == 4) {
								$selection_proportions->print("\t\t\t$selection_total_proportion\n");
								$control_proportions->print("\t\t\t$control_total_proportion\n");
							} else {
								$selection_proportions->print("$selection_88_proportion\t");
								$control_proportions->print("$control_88_proportion\t");
								if (length($control_word) == 3) {
									$selection_proportions->print("\t\t$selection_total_proportion\n");
									$control_proportions->print("\t\t$control_total_proportion\n");
								} else {
									$selection_proportions->print("$selection_89_proportion\t");
									$control_proportions->print("$control_89_proportion\t");
									if (length($control_word) == 2) {
										$selection_proportions->print("\t$selection_total_proportion\n");
										$control_proportions->print("\t$control_total_proportion\n");
									} else {
										$selection_proportions->print("$selection_90_proportion\t$selection_total_proportion\n");
										$control_proportions->print("$control_90_proportion\t$control_total_proportion\n");
									}
								}
							}
						}
					}
				}
			}
		}
	}
	my $occurance = $selection_total_position / ((91 - length($selection_word)) * ($selected_sequence_reads));
	$selection_occurance->print("$selection_word\t$occurance\n");
	$occurance = $control_total_position / ((91 - length($control_word)) * ($control_sequence_reads));
	$control_occurance->print("$control_word\t$occurance\n");
}

$selection_proportions->close();
$control_proportions->close();
$selection_occurance->close();
$control_proportions->close();

sub control_summer {
	$control_hold = shift @control_line;
	chomp $control_hold;
	$control_1_sum = $control_1_sum + $control_hold;
	&control_chomp;
	$control_2_sum = $control_2_sum + $control_hold;
	&control_chomp;
	$control_3_sum = $control_3_sum + $control_hold;
	&control_chomp;
	$control_4_sum = $control_4_sum + $control_hold;
	&control_chomp;
	$control_5_sum = $control_5_sum + $control_hold;
	&control_chomp;
	$control_6_sum = $control_6_sum + $control_hold;
	&control_chomp;
	$control_7_sum = $control_7_sum + $control_hold;
	&control_chomp;
	$control_8_sum = $control_8_sum + $control_hold;
	&control_chomp;
	$control_9_sum = $control_9_sum + $control_hold;
	&control_chomp;
	$control_10_sum = $control_10_sum + $control_hold;
	&control_chomp;
	$control_11_sum = $control_11_sum + $control_hold;
	&control_chomp;
	$control_12_sum = $control_12_sum + $control_hold;
	&control_chomp;
	$control_13_sum = $control_13_sum + $control_hold;
	&control_chomp;
	$control_14_sum = $control_14_sum + $control_hold;
	&control_chomp;
	$control_15_sum = $control_15_sum + $control_hold;
	&control_chomp;
	$control_16_sum = $control_16_sum + $control_hold;
	&control_chomp;
	$control_17_sum = $control_17_sum + $control_hold;
	&control_chomp;
	$control_18_sum = $control_18_sum + $control_hold;
	&control_chomp;
	$control_19_sum = $control_19_sum + $control_hold;
	&control_chomp;
	$control_20_sum = $control_20_sum + $control_hold;
	&control_chomp;
	$control_21_sum = $control_21_sum + $control_hold;
	&control_chomp;
	$control_22_sum = $control_22_sum + $control_hold;
	&control_chomp;
	$control_23_sum = $control_23_sum + $control_hold;
	&control_chomp;
	$control_24_sum = $control_24_sum + $control_hold;
	&control_chomp;
	$control_25_sum = $control_25_sum + $control_hold;
	&control_chomp;
	$control_26_sum = $control_26_sum + $control_hold;
	&control_chomp;
	$control_27_sum = $control_27_sum + $control_hold;
	&control_chomp;
	$control_28_sum = $control_28_sum + $control_hold;
	&control_chomp;
	$control_29_sum = $control_29_sum + $control_hold;
	&control_chomp;
	$control_30_sum = $control_30_sum + $control_hold;
	&control_chomp;
	$control_31_sum = $control_31_sum + $control_hold;
	&control_chomp;
	$control_32_sum = $control_32_sum + $control_hold;
	&control_chomp;
	$control_33_sum = $control_33_sum + $control_hold;
	&control_chomp;
	$control_34_sum = $control_34_sum + $control_hold;
	&control_chomp;
	$control_35_sum = $control_35_sum + $control_hold;
	&control_chomp;
	$control_36_sum = $control_36_sum + $control_hold;
	&control_chomp;
	$control_37_sum = $control_37_sum + $control_hold;
	&control_chomp;
	$control_38_sum = $control_38_sum + $control_hold;
	&control_chomp;
	$control_39_sum = $control_39_sum + $control_hold;
	&control_chomp;
	$control_40_sum = $control_40_sum + $control_hold;
	&control_chomp;
	$control_41_sum = $control_41_sum + $control_hold;
	&control_chomp;
	$control_42_sum = $control_42_sum + $control_hold;
	&control_chomp;
	$control_43_sum = $control_43_sum + $control_hold;
	&control_chomp;
	$control_44_sum = $control_44_sum + $control_hold;
	&control_chomp;
	$control_45_sum = $control_45_sum + $control_hold;
	&control_chomp;
	$control_46_sum = $control_46_sum + $control_hold;
	&control_chomp;
	$control_47_sum = $control_47_sum + $control_hold;
	&control_chomp;
	$control_48_sum = $control_48_sum + $control_hold;
	&control_chomp;
	$control_49_sum = $control_49_sum + $control_hold;
	&control_chomp;
	$control_50_sum = $control_50_sum + $control_hold;
	&control_chomp;
	$control_51_sum = $control_51_sum + $control_hold;
	&control_chomp;
	$control_52_sum = $control_52_sum + $control_hold;
	&control_chomp;
	$control_53_sum = $control_53_sum + $control_hold;
	&control_chomp;
	$control_54_sum = $control_54_sum + $control_hold;
	&control_chomp;
	$control_55_sum = $control_55_sum + $control_hold;
	&control_chomp;
	$control_56_sum = $control_56_sum + $control_hold;
	&control_chomp;
	$control_57_sum = $control_57_sum + $control_hold;
	&control_chomp;
	$control_58_sum = $control_58_sum + $control_hold;
	&control_chomp;
	$control_59_sum = $control_59_sum + $control_hold;
	&control_chomp;
	$control_60_sum = $control_60_sum + $control_hold;
	&control_chomp;
	$control_61_sum = $control_61_sum + $control_hold;
	&control_chomp;
	$control_62_sum = $control_62_sum + $control_hold;
	&control_chomp;
	$control_63_sum = $control_63_sum + $control_hold;
	&control_chomp;
	$control_64_sum = $control_64_sum + $control_hold;
	&control_chomp;
	$control_65_sum = $control_65_sum + $control_hold;
	&control_chomp;
	$control_66_sum = $control_66_sum + $control_hold;
	&control_chomp;
	$control_67_sum = $control_67_sum + $control_hold;
	&control_chomp;
	$control_68_sum = $control_68_sum + $control_hold;
	&control_chomp;
	$control_69_sum = $control_69_sum + $control_hold;
	&control_chomp;
	$control_70_sum = $control_70_sum + $control_hold;
	&control_chomp;
	$control_71_sum = $control_71_sum + $control_hold;
	&control_chomp;
	$control_72_sum = $control_72_sum + $control_hold;
	&control_chomp;
	$control_73_sum = $control_73_sum + $control_hold;
	&control_chomp;
	$control_74_sum = $control_74_sum + $control_hold;
	&control_chomp;
	$control_75_sum = $control_75_sum + $control_hold;
	&control_chomp;
	$control_76_sum = $control_76_sum + $control_hold;
	&control_chomp;
	$control_77_sum = $control_77_sum + $control_hold;
	&control_chomp;
	$control_78_sum = $control_78_sum + $control_hold;
	&control_chomp;
	$control_79_sum = $control_79_sum + $control_hold;
	&control_chomp;
	$control_80_sum = $control_80_sum + $control_hold;
	&control_chomp;
	$control_81_sum = $control_81_sum + $control_hold;
	&control_chomp;
	$control_82_sum = $control_82_sum + $control_hold;
	&control_chomp;
	$control_83_sum = $control_83_sum + $control_hold;
	&control_chomp;
	$control_84_sum = $control_84_sum + $control_hold;
	&control_chomp;
	$control_85_sum = $control_85_sum + $control_hold;
	&control_chomp;
	$control_86_sum = $control_86_sum + $control_hold;
	&control_chomp;
	$control_87_sum = $control_87_sum + $control_hold;
	&control_chomp;
	$control_88_sum = $control_88_sum + $control_hold;
	&control_chomp;
	$control_89_sum = $control_89_sum + $control_hold;
	&control_chomp;
	$control_90_sum = $control_90_sum + $control_hold;
	&control_chomp;
	$control_total_sum = $control_total_sum + $control_hold;
}

sub control_chomp {
	$control_hold = shift @control_line;
	chomp $control_hold;
}

sub selection_summer {
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
	$selection_1_sum = $selection_1_sum + $selection_hold;
	&selection_chomp;
	$selection_2_sum = $selection_2_sum + $selection_hold;
	&selection_chomp;
	$selection_3_sum = $selection_3_sum + $selection_hold;
	&selection_chomp;
	$selection_4_sum = $selection_4_sum + $selection_hold;
	&selection_chomp;
	$selection_5_sum = $selection_5_sum + $selection_hold;
	&selection_chomp;
	$selection_6_sum = $selection_6_sum + $selection_hold;
	&selection_chomp;
	$selection_7_sum = $selection_7_sum + $selection_hold;
	&selection_chomp;
	$selection_8_sum = $selection_8_sum + $selection_hold;
	&selection_chomp;
	$selection_9_sum = $selection_9_sum + $selection_hold;
	&selection_chomp;
	$selection_10_sum = $selection_10_sum + $selection_hold;
	&selection_chomp;
	$selection_11_sum = $selection_11_sum + $selection_hold;
	&selection_chomp;
	$selection_12_sum = $selection_12_sum + $selection_hold;
	&selection_chomp;
	$selection_13_sum = $selection_13_sum + $selection_hold;
	&selection_chomp;
	$selection_14_sum = $selection_14_sum + $selection_hold;
	&selection_chomp;
	$selection_15_sum = $selection_15_sum + $selection_hold;
	&selection_chomp;
	$selection_16_sum = $selection_16_sum + $selection_hold;
	&selection_chomp;
	$selection_17_sum = $selection_17_sum + $selection_hold;
	&selection_chomp;
	$selection_18_sum = $selection_18_sum + $selection_hold;
	&selection_chomp;
	$selection_19_sum = $selection_19_sum + $selection_hold;
	&selection_chomp;
	$selection_20_sum = $selection_20_sum + $selection_hold;
	&selection_chomp;
	$selection_21_sum = $selection_21_sum + $selection_hold;
	&selection_chomp;
	$selection_22_sum = $selection_22_sum + $selection_hold;
	&selection_chomp;
	$selection_23_sum = $selection_23_sum + $selection_hold;
	&selection_chomp;
	$selection_24_sum = $selection_24_sum + $selection_hold;
	&selection_chomp;
	$selection_25_sum = $selection_25_sum + $selection_hold;
	&selection_chomp;
	$selection_26_sum = $selection_26_sum + $selection_hold;
	&selection_chomp;
	$selection_27_sum = $selection_27_sum + $selection_hold;
	&selection_chomp;
	$selection_28_sum = $selection_28_sum + $selection_hold;
	&selection_chomp;
	$selection_29_sum = $selection_29_sum + $selection_hold;
	&selection_chomp;
	$selection_30_sum = $selection_30_sum + $selection_hold;
	&selection_chomp;
	$selection_31_sum = $selection_31_sum + $selection_hold;
	&selection_chomp;
	$selection_32_sum = $selection_32_sum + $selection_hold;
	&selection_chomp;
	$selection_33_sum = $selection_33_sum + $selection_hold;
	&selection_chomp;
	$selection_34_sum = $selection_34_sum + $selection_hold;
	&selection_chomp;
	$selection_35_sum = $selection_35_sum + $selection_hold;
	&selection_chomp;
	$selection_36_sum = $selection_36_sum + $selection_hold;
	&selection_chomp;
	$selection_37_sum = $selection_37_sum + $selection_hold;
	&selection_chomp;
	$selection_38_sum = $selection_38_sum + $selection_hold;
	&selection_chomp;
	$selection_39_sum = $selection_39_sum + $selection_hold;
	&selection_chomp;
	$selection_40_sum = $selection_40_sum + $selection_hold;
	&selection_chomp;
	$selection_41_sum = $selection_41_sum + $selection_hold;
	&selection_chomp;
	$selection_42_sum = $selection_42_sum + $selection_hold;
	&selection_chomp;
	$selection_43_sum = $selection_43_sum + $selection_hold;
	&selection_chomp;
	$selection_44_sum = $selection_44_sum + $selection_hold;
	&selection_chomp;
	$selection_45_sum = $selection_45_sum + $selection_hold;
	&selection_chomp;
	$selection_46_sum = $selection_46_sum + $selection_hold;
	&selection_chomp;
	$selection_47_sum = $selection_47_sum + $selection_hold;
	&selection_chomp;
	$selection_48_sum = $selection_48_sum + $selection_hold;
	&selection_chomp;
	$selection_49_sum = $selection_49_sum + $selection_hold;
	&selection_chomp;
	$selection_50_sum = $selection_50_sum + $selection_hold;
	&selection_chomp;
	$selection_51_sum = $selection_51_sum + $selection_hold;
	&selection_chomp;
	$selection_52_sum = $selection_52_sum + $selection_hold;
	&selection_chomp;
	$selection_53_sum = $selection_53_sum + $selection_hold;
	&selection_chomp;
	$selection_54_sum = $selection_54_sum + $selection_hold;
	&selection_chomp;
	$selection_55_sum = $selection_55_sum + $selection_hold;
	&selection_chomp;
	$selection_56_sum = $selection_56_sum + $selection_hold;
	&selection_chomp;
	$selection_57_sum = $selection_57_sum + $selection_hold;
	&selection_chomp;
	$selection_58_sum = $selection_58_sum + $selection_hold;
	&selection_chomp;
	$selection_59_sum = $selection_59_sum + $selection_hold;
	&selection_chomp;
	$selection_60_sum = $selection_60_sum + $selection_hold;
	&selection_chomp;
	$selection_61_sum = $selection_61_sum + $selection_hold;
	&selection_chomp;
	$selection_62_sum = $selection_62_sum + $selection_hold;
	&selection_chomp;
	$selection_63_sum = $selection_63_sum + $selection_hold;
	&selection_chomp;
	$selection_64_sum = $selection_64_sum + $selection_hold;
	&selection_chomp;
	$selection_65_sum = $selection_65_sum + $selection_hold;
	&selection_chomp;
	$selection_66_sum = $selection_66_sum + $selection_hold;
	&selection_chomp;
	$selection_67_sum = $selection_67_sum + $selection_hold;
	&selection_chomp;
	$selection_68_sum = $selection_68_sum + $selection_hold;
	&selection_chomp;
	$selection_69_sum = $selection_69_sum + $selection_hold;
	&selection_chomp;
	$selection_70_sum = $selection_70_sum + $selection_hold;
	&selection_chomp;
	$selection_71_sum = $selection_71_sum + $selection_hold;
	&selection_chomp;
	$selection_72_sum = $selection_72_sum + $selection_hold;
	&selection_chomp;
	$selection_73_sum = $selection_73_sum + $selection_hold;
	&selection_chomp;
	$selection_74_sum = $selection_74_sum + $selection_hold;
	&selection_chomp;
	$selection_75_sum = $selection_75_sum + $selection_hold;
	&selection_chomp;
	$selection_76_sum = $selection_76_sum + $selection_hold;
	&selection_chomp;
	$selection_77_sum = $selection_77_sum + $selection_hold;
	&selection_chomp;
	$selection_78_sum = $selection_78_sum + $selection_hold;
	&selection_chomp;
	$selection_79_sum = $selection_79_sum + $selection_hold;
	&selection_chomp;
	$selection_80_sum = $selection_80_sum + $selection_hold;
	&selection_chomp;
	$selection_81_sum = $selection_81_sum + $selection_hold;
	&selection_chomp;
	$selection_82_sum = $selection_82_sum + $selection_hold;
	&selection_chomp;
	$selection_83_sum = $selection_83_sum + $selection_hold;
	&selection_chomp;
	$selection_84_sum = $selection_84_sum + $selection_hold;
	&selection_chomp;
	$selection_85_sum = $selection_85_sum + $selection_hold;
	&selection_chomp;
	$selection_86_sum = $selection_86_sum + $selection_hold;
	&selection_chomp;
	$selection_87_sum = $selection_87_sum + $selection_hold;
	&selection_chomp;
	$selection_88_sum = $selection_88_sum + $selection_hold;
	&selection_chomp;
	$selection_89_sum = $selection_89_sum + $selection_hold;
	&selection_chomp;
	$selection_90_sum = $selection_90_sum + $selection_hold;
	&selection_chomp;
	$selection_total_sum = $selection_total_sum + $selection_hold;
}

sub selection_chomp {
	$selection_hold = shift @selection_line;
	chomp $selection_hold;
} 