#!/usr/bin/perl
#simpalign.plx
#matches best (highest) exact overlap (from 100bp to 1bp) of two 100bp sequences from one DNA with barcodes and then sorts the contigs into files by total length of the SOLEXA sequenced DNA  (replaces 3aligner through 5lengther)
use warnings;
use strict;
use 5.010;

print "Enter a merged, stripper-formated DNA file (\"merged_stripped_example.txt\"): ";

my $file = <STDIN>;
chomp $file;

my $align_success = 0;
my $align_attempt = 0;
my $fail_align = 0;
my $overlap;
my @overlaps = (0..100);
my $lapentry;
my %lengther;
for $lapentry (@overlaps) {
	$lengther {$lapentry} = 0;
}
@overlaps = reverse @overlaps;

print "Enter output identification name (\"contig_{length}_IDENTIFICATION\"): ";

my $outfile = <STDIN>;
chomp $outfile;

my $expected = 0;

open DATA, "<", "$file" or die "ERROR: No file(s) of that name exists in this folder.\n";	#input file

open UNTRIM, ">", "contig_140_$outfile\_untrim" or die "ERROR: Cannot create alignment output file.\n";	#output file for untrimmed check created

#open OUT100, ">", "contig_100_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT99, ">", "contig_101_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT98, ">", "contig_102_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT97, ">", "contig_103_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT96, ">", "contig_104_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT95, ">", "contig_105_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT94, ">", "contig_106_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT93, ">", "contig_107_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT92, ">", "contig_108_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT91, ">", "contig_109_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT90, ">", "contig_110_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT89, ">", "contig_111_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT88, ">", "contig_112_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT87, ">", "contig_113_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT86, ">", "contig_114_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT85, ">", "contig_115_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT84, ">", "contig_116_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT83, ">", "contig_117_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT82, ">", "contig_118_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT81, ">", "contig_119_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT80, ">", "contig_120_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT79, ">", "contig_121_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT78, ">", "contig_122_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT77, ">", "contig_123_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT76, ">", "contig_124_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT75, ">", "contig_125_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT74, ">", "contig_126_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT73, ">", "contig_127_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT72, ">", "contig_128_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT71, ">", "contig_129_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT70, ">", "contig_130_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT69, ">", "contig_131_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT68, ">", "contig_132_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT67, ">", "contig_133_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT66, ">", "contig_134_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT65, ">", "contig_135_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT64, ">", "contig_136_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT63, ">", "contig_137_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT62, ">", "contig_138_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT61, ">", "contig_139_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
open OUT60, ">", "contig_140_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT59, ">", "contig_141_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT58, ">", "contig_142_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT57, ">", "contig_143_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT56, ">", "contig_144_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT55, ">", "contig_145_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT54, ">", "contig_146_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT53, ">", "contig_147_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT52, ">", "contig_148_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT51, ">", "contig_149_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT50, ">", "contig_150_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT49, ">", "contig_151_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT48, ">", "contig_152_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT47, ">", "contig_153_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT46, ">", "contig_154_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT45, ">", "contig_155_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT44, ">", "contig_156_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT43, ">", "contig_157_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT42, ">", "contig_158_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT41, ">", "contig_159_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT40, ">", "contig_160_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT39, ">", "contig_161_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT38, ">", "contig_162_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT37, ">", "contig_163_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT36, ">", "contig_164_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT35, ">", "contig_165_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT34, ">", "contig_166_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT33, ">", "contig_167_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT32, ">", "contig_168_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT31, ">", "contig_169_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT30, ">", "contig_170_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT29, ">", "contig_171_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT28, ">", "contig_172_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT27, ">", "contig_173_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT26, ">", "contig_174_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT25, ">", "contig_175_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT24, ">", "contig_176_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT23, ">", "contig_177_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT22, ">", "contig_178_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT21, ">", "contig_179_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT20, ">", "contig_180_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT19, ">", "contig_181_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT18, ">", "contig_182_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT17, ">", "contig_183_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT16, ">", "contig_184_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT15, ">", "contig_185_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT14, ">", "contig_186_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT13, ">", "contig_187_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT12, ">", "contig_188_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT11, ">", "contig_189_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT10, ">", "contig_190_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT9, ">", "contig_191_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT8, ">", "contig_192_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT7, ">", "contig_193_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT6, ">", "contig_194_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT5, ">", "contig_195_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT4, ">", "contig_196_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT3, ">", "contig_197_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT2, ">", "contig_198_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT1, ">", "contig_199_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
#open OUT0, ">", "contig_none_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
  
while (<DATA>) {
#Split each line of the merged file into two files for alignment
	my @line = split/\t/;
	my $sequence_2 = pop @line;
	my $sequence_1 = pop @line;
	my $beadid = pop @line;
	chomp $sequence_2;
	my $foundit = 0;
	COUNTDOWN: for $overlap (@overlaps) {
		my $overhang = 100 - $overlap;
		$sequence_1 =~ /^(\w{$overhang})(\w{$overlap})$/;
		my $front = $1;
		my $mid = $2;
		if ($sequence_2 =~ /^$mid(\w{$overhang})$/) {
			my $back = $1;
			my $contig = $front.$mid.$back;
			my $trim = substr $contig, 24, -26;
			$lengther{$overlap} = $lengther{$overlap} + 1; #add one to length tracker of given length
			$align_success++;
			if ($overlap == 100) {
#				print OUT100 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 99) {
#				print OUT99 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 98) {
#				print OUT98 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 97) {
#				print OUT97 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 96) {
#				print OUT96 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 95) {
#				print OUT95 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 94) {
#				print OUT94 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 93) {
#				print OUT93 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 92) {
#				print OUT92 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 91) {
#				print OUT91 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 90) {
#				print OUT90 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 89) {
#				print OUT89 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 88) {
#				print OUT88 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 87) {
#				print OUT87 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 86) {
#				print OUT86 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 85) {
#				print OUT85 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 84) {
#				print OUT84 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 83) {
#				print OUT83 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 82) {
#				print OUT82 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 81) {
#				print OUT81 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 80) {
#				print OUT80 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 79) {
#				print OUT79 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 78) {
#				print OUT78 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 77) {
#				print OUT77 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 76) {
#				print OUT76 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 75) {
#				print OUT75 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 74) {
#				print UNTRIM "$beadid\t$contig\n";
#				print OUT74 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 73) {
#				print OUT73 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 72) {
#				print OUT72 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 71) {
#				print OUT71 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 70) {
#				print OUT70 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 69) {
#				print OUT69 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 68) {
#				print OUT68 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 67) {
#				print OUT67 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 66) {
#				print OUT66 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 65) {
#				print OUT65 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 64) {
#				print OUT64 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 63) {
#				print OUT63 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 62) {
#				print OUT62 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 61) {
#				print OUT61 "$beadid\t$trim\n";
				$foundit++;
			} elsif ($overlap == 60) {
				print UNTRIM "$beadid\t$contig\n";
				print OUT60 "$beadid\t$trim\n";
				$foundit++;
				$expected++;
				}
#			} elsif ($overlap == 59) {
#				print OUT59 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 58) {
#				print OUT58 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 57) {
#				print OUT57 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 56) {
#				print OUT56 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 55) {
#				print OUT55 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 54) {
#				print OUT54 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 53) {
#				print OUT53 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 52) {
#				print OUT52 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 51) {
#				print OUT51 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 50) {
#				print OUT50 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 49) {
#				print OUT49 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 48) {
#				print OUT48 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 47) {
#				print OUT47 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 46) {
#				print OUT46 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 45) {
#				print OUT45 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 44) {
#				print OUT44 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 43) {
#				print OUT43 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 42) {
#				print OUT42 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 41) {
#				print OUT41 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 40) {
#				print OUT40 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 39) {
#				print OUT39 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 38) {
#				print OUT38 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 37) {
#				print OUT37 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 36) {
#				print OUT36 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 35) {
#				print OUT35 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 34) {
#				print OUT34 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 33) {
#				print OUT33 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 32) {
#				print OUT32 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 31) {
#				print OUT31 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 30) {
#				print OUT30 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 29) {
#				print OUT29 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 28) {
#				print OUT28 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 27) {
#				print OUT27 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 26) {
#				print OUT26 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 25) {
#				print OUT25 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 24) {
#				print OUT24 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 23) {
#				print OUT23 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 22) {
#				print OUT22 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 21) {
#				print OUT21 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 20) {
#				print OUT20 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 19) {
#				print OUT19 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 18) {
#				print OUT18 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 17) {
#				print OUT17 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 16) {
#				print OUT16 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 15) {
#				print OUT15 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 14) {
#				print OUT14 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 13) {
#				print OUT13 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 12) {
#				print OUT12 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 11) {
#				print OUT11 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 10) {
#				print OUT10 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 9) {
#				print OUT9 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 8) {
#				print OUT8 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 7) {
#				print OUT7 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 6) {
#				print OUT6 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 5) {
#				print OUT5 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 4) {
#				print OUT4 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 3) {
#				print OUT3 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 2) {
#				print OUT2 "$beadid\t$trim\n";
#				$foundit++;
#			} elsif ($overlap == 1) {
#				print OUT1 "$beadid\t$trim\n";
#				$foundit++;
#			}
#				else {
#				print OUT0 "$beadid\t$sequence_1\t$sequence_2\n";
#				$foundit++;
#				}
			}
		last COUNTDOWN if ($foundit ==1);
		}
	$align_attempt++;
	if (($align_attempt % 1000) == 0) {
		print "$align_attempt\n";
	}
}

close DATA;

print "$file had $align_success / $align_attempt successful alignments generated.\n$expected contigs were of 140bp in length.\n";

#close OUT100;
#close OUT99;
#close OUT98;
#close OUT97;
#close OUT96;
#close OUT95;
#close OUT94;
#close OUT93;
#close OUT92;
#close OUT91;
#close OUT90;
#close OUT89;
#close OUT88;
#close OUT87;
#close OUT86;
#close OUT85;
#close OUT84;
#close OUT83;
#close OUT82;
#close OUT81;
#close OUT80;
#close OUT79;
#close OUT78;
#close OUT77;
#close OUT76;
#close OUT75;
#close OUT74;
#close OUT73;
#close OUT72;
#close OUT71;
#close OUT70;
#close OUT69;
#close OUT68;
#close OUT67;
#close OUT66;
#close OUT65;
#close OUT64;
#close OUT63;
#close OUT62;
#close OUT61;
close OUT60;
close UNTRIM;
#close OUT59;
#close OUT58;
#close OUT57;
#close OUT56;
#close OUT55;
#close OUT54;
#close OUT53;
#close OUT52;
#close OUT51;
#close OUT50;
#close OUT49;
#close OUT48;
#close OUT47;
#close OUT46;
#close OUT45;
#close OUT44;
#close OUT43;
#close OUT42;
#close OUT41;
#close OUT40;
#close OUT39;
#close OUT38;
#close OUT37;
#close OUT36;
#close OUT35;
#close OUT34;
#close OUT33;
#close OUT32;
#close OUT31;
#close OUT30;
#close OUT29;
#close OUT28;
#close OUT27;
#close OUT26;
#close OUT25;
#close OUT24;
#close OUT23;
#close OUT22;
#close OUT21;
#close OUT20;
#close OUT19;
#close OUT18;
#close OUT17;
#close OUT16;
#close OUT15;
#close OUT14;
#close OUT13;
#close OUT12;
#close OUT11;
#close OUT10;
#close OUT9;
#close OUT8;
#close OUT7;
#close OUT6;
#close OUT5;
#close OUT4;
#close OUT3;
#close OUT2;
#close OUT1;
#close OUT0;

#open BRIEF, ">", "contig_overview_$outfile" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created

#for (sort {$a <=> $b} keys(%lengther)) {
#	if ($_ == 0) {
#		print BRIEF "$lengther{$_} sequence pairs have no discernable overlap.\n";
#	} else {
#		print BRIEF "$lengther{$_} contigs are ", (200 - $_), " bps in length.\n";
#	}
#}

#close BRIEF;

#for (keys(%lengther)) {
#	if ($lengther{$_} == 0) {
#		my $deletefile;
#		if ($_ == 0) {
#			$deletefile = "contig_none_$outfile";
#		} else {
#			$deletefile = "contig_".(200 - $_)."_$outfile";
#		}
#		unlink $deletefile;
#	}
#}

#print "$file had $align_success / $align_attempt successful alignments generated.\nOverview of sorting saved to contig_overview_$outfile.\n"; 