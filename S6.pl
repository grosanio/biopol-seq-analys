#!/usr/bin/perl
#6comberv2.plx
#recombines separately finished wordcounted files and uniques them for finishing; also separates the combined counts into separate files based on word length
use warnings;
use strict;
use 5.010;

print "Enter wordcounted files (\"wordcount_seqcounts_contig_126_FIRSTexample.txt wordcount_seqcounts_contig_126_SECONDexample.txt wordcount_seqcounts_contig_126_THIRDexample.txt\"): ";

my $files = <STDIN>;
chomp $files;
my @input = split / /, $files;
my $tracker = 0;

open HOLD1, ">", "HOLD_01_$input[0]";
open HOLD2, ">", "HOLD_02_$input[0]";
open HOLD3, ">", "HOLD_03_$input[0]";
open HOLD4, ">", "HOLD_04_$input[0]";
open HOLD5, ">", "HOLD_05_$input[0]";
open HOLD6, ">", "HOLD_06_$input[0]";
open HOLD7, ">", "HOLD_07_$input[0]";
open HOLD8, ">", "HOLD_08_$input[0]";
open HOLD9, ">", "HOLD_09_$input[0]";
open HOLD10, ">", "HOLD_10_$input[0]";

for my $check (@input) {
	my $holder = $check;
	open DATA, "<", "$holder" or die "ERROR: No file(s) of that name exists in this folder.\n";
	while (<DATA>) {
		$tracker++;
		chomp;
		my @line = split/\t/;
		chomp @line;
		my $sequence = $line[0];
		chomp $sequence;
		if (length ($sequence) == 1) {
			print HOLD1 "$_"."\n";
		} elsif (length ($sequence) == 2) {
			print HOLD2 "$_"."\n";
		} elsif (length ($sequence) == 3) {
			print HOLD3 "$_"."\n";
		} elsif (length ($sequence) == 4) {
			print HOLD4 "$_"."\n";
		} elsif (length ($sequence) == 5) {
			print HOLD5 "$_"."\n";
		} elsif (length ($sequence) == 6) {
			print HOLD6 "$_"."\n";
		} elsif (length ($sequence) == 7) {
			print HOLD7 "$_"."\n";
		} elsif (length ($sequence) == 8) {
			print HOLD8 "$_"."\n";
		} elsif (length ($sequence) == 9) {
			print HOLD9 "$_"."\n";
		} elsif (length ($sequence) == 10) {
			print HOLD10 "$_"."\n";
		}
	}
	close DATA;
}

close HOLD1;
close HOLD2;
close HOLD3;
close HOLD4;
close HOLD5;
close HOLD6;
close HOLD7;
close HOLD8;
close HOLD9;
close HOLD10;

print "$tracker lines combined in hold files.\n";

my %zoo; #hash for counting
my $zookeeper; #counts the number of contigs read

open HOLD1, "<", "HOLD_01_$input[0]";
while (<HOLD1>) {
	my @line = split/\t/;
	my $sequence = shift @line;
	chomp $sequence;
	chomp @line;
	unless ($sequence =~ m/N/) {
		if (exists $zoo{$sequence}) {
			for (0..90) {
				$zoo{$sequence}[$_] = $zoo{$sequence}[$_] + $line[$_];
			}#add count to count for a particular contig sequence
		} else {
			for (0..90) {
				$zoo{$sequence}[$_] = $line[$_];
			}
		}
	}
	$zookeeper++; #add one to the tracker
	if (($zookeeper % 10000) == 0) {
		print "$zookeeper\n";
	}
}
close HOLD1;

open OUT1, ">", "combed_01_$input[0]";
for my $animals (sort keys %zoo) {
    print OUT1 "$animals";
    for (@{$zoo{$animals}}) {
    	print OUT1 "\t$_";
	}
	print OUT1 "\n";
} #print the sequence info to file
close OUT1;

open SIMPLEOUT1, ">", "simple_combed_01_$input[0]";

&simplify;

for my $animals (sort keys %zoo) {
    print SIMPLEOUT1 "$animals";
    for (@{$zoo{$animals}}) {
    	print SIMPLEOUT1 "\t$_";
	}
	print SIMPLEOUT1 "\n";
} #print the sequence info to file

&clear_zoo;

print "One base words counted.\n";

open HOLD2, "<", "HOLD_02_$input[0]";
while (<HOLD2>) {
	my @line = split/\t/;
	my $sequence = shift @line;
	chomp $sequence;
	chomp @line;
	unless ($sequence =~ m/N/) {
		if (exists $zoo{$sequence}) {
			for (0..90) {
				$zoo{$sequence}[$_] = $zoo{$sequence}[$_] + $line[$_];
			}#add count to count for a particular contig sequence
		} else {
			for (0..90) {
				$zoo{$sequence}[$_] = $line[$_];
			}
		}
	}
	$zookeeper++; #add one to the tracker
	if (($zookeeper % 10000) == 0) {
		print "$zookeeper\n";
	}
}
close HOLD2;

open OUT2, ">", "combed_02_$input[0]";
for my $animals (sort keys %zoo) {
    print OUT2 "$animals";
    for (@{$zoo{$animals}}) {
    	print OUT2 "\t$_";
	}
	print OUT2 "\n";
} #print the sequence info to file
close OUT2;

open SIMPLEOUT2, ">", "simple_combed_02_$input[0]";

&simplify;

for my $animals (sort keys %zoo) {
    print SIMPLEOUT2 "$animals";
    for (@{$zoo{$animals}}) {
    	print SIMPLEOUT2 "\t$_";
	}
	print SIMPLEOUT2 "\n";
} #print the sequence info to file

&clear_zoo;

print "Two base words counted.\n";

open HOLD3, "<", "HOLD_03_$input[0]";
while (<HOLD3>) {
	my @line = split/\t/;
	my $sequence = shift @line;
	chomp $sequence;
	chomp @line;
	unless ($sequence =~ m/N/) {
		if (exists $zoo{$sequence}) {
			for (0..90) {
				$zoo{$sequence}[$_] = $zoo{$sequence}[$_] + $line[$_];
			}#add count to count for a particular contig sequence
		} else {
			for (0..90) {
				$zoo{$sequence}[$_] = $line[$_];
			}
		}
	}
	$zookeeper++; #add one to the tracker
	if (($zookeeper % 10000) == 0) {
		print "$zookeeper\n";
	}
}
close HOLD3;

open OUT3, ">", "combed_03_$input[0]";
for my $animals (sort keys %zoo) {
    print OUT3 "$animals";
    for (@{$zoo{$animals}}) {
    	print OUT3 "\t$_";
	}
	print OUT3 "\n";
} #print the sequence info to file
close OUT3;

open SIMPLEOUT3, ">", "simple_combed_03_$input[0]";

&simplify;

for my $animals (sort keys %zoo) {
    print SIMPLEOUT3 "$animals";
    for (@{$zoo{$animals}}) {
    	print SIMPLEOUT3 "\t$_";
	}
	print SIMPLEOUT3 "\n";
} #print the sequence info to file

&clear_zoo;

print "Three base words counted.\n";

open HOLD4, "<", "HOLD_04_$input[0]";
while (<HOLD4>) {
	my @line = split/\t/;
	my $sequence = shift @line;
	chomp $sequence;
	chomp @line;
	unless ($sequence =~ m/N/) {
		if (exists $zoo{$sequence}) {
			for (0..90) {
				$zoo{$sequence}[$_] = $zoo{$sequence}[$_] + $line[$_];
			}#add count to count for a particular contig sequence
		} else {
			for (0..90) {
				$zoo{$sequence}[$_] = $line[$_];
			}
		}
	}
	$zookeeper++; #add one to the tracker
	if (($zookeeper % 10000) == 0) {
		print "$zookeeper\n";
	}
}
close HOLD4;

open OUT4, ">", "combed_04_$input[0]";
for my $animals (sort keys %zoo) {
    print OUT4 "$animals";
    for (@{$zoo{$animals}}) {
    	print OUT4 "\t$_";
	}
	print OUT4 "\n";
} #print the sequence info to file
close OUT4;

open SIMPLEOUT4, ">", "simple_combed_04_$input[0]";

&simplify;

for my $animals (sort keys %zoo) {
    print SIMPLEOUT4 "$animals";
    for (@{$zoo{$animals}}) {
    	print SIMPLEOUT4 "\t$_";
	}
	print SIMPLEOUT4 "\n";
} #print the sequence info to file

&clear_zoo;

print "Four base words counted.\n";

open HOLD5, "<", "HOLD_05_$input[0]";
while (<HOLD5>) {
	my @line = split/\t/;
	my $sequence = shift @line;
	chomp $sequence;
	chomp @line;
	unless ($sequence =~ m/N/) {
		if (exists $zoo{$sequence}) {
			for (0..90) {
				$zoo{$sequence}[$_] = $zoo{$sequence}[$_] + $line[$_];
			}#add count to count for a particular contig sequence
		} else {
			for (0..90) {
				$zoo{$sequence}[$_] = $line[$_];
			}
		}
	}
	$zookeeper++; #add one to the tracker
	if (($zookeeper % 10000) == 0) {
		print "$zookeeper\n";
	}
}
close HOLD5;

open OUT5, ">", "combed_05_$input[0]";
for my $animals (sort keys %zoo) {
    print OUT5 "$animals";
    for (@{$zoo{$animals}}) {
    	print OUT5 "\t$_";
	}
	print OUT5 "\n";
} #print the sequence info to file
close OUT5;

open SIMPLEOUT5, ">", "simple_combed_05_$input[0]";

&simplify;

for my $animals (sort keys %zoo) {
    print SIMPLEOUT5 "$animals";
    for (@{$zoo{$animals}}) {
    	print SIMPLEOUT5 "\t$_";
	}
	print SIMPLEOUT5 "\n";
} #print the sequence info to file

&clear_zoo;

print "Five base words counted.\n";

open HOLD6, "<", "HOLD_06_$input[0]";
while (<HOLD6>) {
	my @line = split/\t/;
	my $sequence = shift @line;
	chomp $sequence;
	chomp @line;
	unless ($sequence =~ m/N/) {
		if (exists $zoo{$sequence}) {
			for (0..90) {
				$zoo{$sequence}[$_] = $zoo{$sequence}[$_] + $line[$_];
			}#add count to count for a particular contig sequence
		} else {
			for (0..90) {
				$zoo{$sequence}[$_] = $line[$_];
			}
		}
	}
	$zookeeper++; #add one to the tracker
	if (($zookeeper % 10000) == 0) {
		print "$zookeeper\n";
	}
}
close HOLD6;

open OUT6, ">", "combed_06_$input[0]";
for my $animals (sort keys %zoo) {
    print OUT6 "$animals";
    for (@{$zoo{$animals}}) {
    	print OUT6 "\t$_";
	}
	print OUT6 "\n";
} #print the sequence info to file
close OUT6;

open SIMPLEOUT6, ">", "simple_combed_06_$input[0]";

&simplify;

for my $animals (sort keys %zoo) {
    print SIMPLEOUT6 "$animals";
    for (@{$zoo{$animals}}) {
    	print SIMPLEOUT6 "\t$_";
	}
	print SIMPLEOUT6 "\n";
} #print the sequence info to file

&clear_zoo;

print "Six base words counted.\n";

open HOLD7, "<", "HOLD_07_$input[0]";
while (<HOLD7>) {
	my @line = split/\t/;
	my $sequence = shift @line;
	chomp $sequence;
	chomp @line;
	unless ($sequence =~ m/N/) {
		if (exists $zoo{$sequence}) {
			for (0..90) {
				$zoo{$sequence}[$_] = $zoo{$sequence}[$_] + $line[$_];
			}#add count to count for a particular contig sequence
		} else {
			for (0..90) {
				$zoo{$sequence}[$_] = $line[$_];
			}
		}
	}
	$zookeeper++; #add one to the tracker
	if (($zookeeper % 10000) == 0) {
		print "$zookeeper\n";
	}
}
close HOLD7;

open OUT7, ">", "combed_07_$input[0]";
for my $animals (sort keys %zoo) {
    print OUT7 "$animals";
    for (@{$zoo{$animals}}) {
    	print OUT7 "\t$_";
	}
	print OUT7 "\n";
} #print the sequence info to file
close OUT7;

open SIMPLEOUT7, ">", "simple_combed_07_$input[0]";

&simplify;

for my $animals (sort keys %zoo) {
    print SIMPLEOUT7 "$animals";
    for (@{$zoo{$animals}}) {
    	print SIMPLEOUT7 "\t$_";
	}
	print SIMPLEOUT7 "\n";
} #print the sequence info to file

&clear_zoo;

print "Seven base words counted.\n";

open HOLD8, "<", "HOLD_08_$input[0]";
while (<HOLD8>) {
	my @line = split/\t/;
	my $sequence = shift @line;
	chomp $sequence;
	chomp @line;
	unless ($sequence =~ m/N/) {
		if (exists $zoo{$sequence}) {
			for (0..90) {
				$zoo{$sequence}[$_] = $zoo{$sequence}[$_] + $line[$_];
			}#add count to count for a particular contig sequence
		} else {
			for (0..90) {
				$zoo{$sequence}[$_] = $line[$_];
			}
		}
	}
	$zookeeper++; #add one to the tracker
	if (($zookeeper % 10000) == 0) {
		print "$zookeeper\n";
	}
}
close HOLD8;

open OUT8, ">", "combed_08_$input[0]";
for my $animals (sort keys %zoo) {
    print OUT8 "$animals";
    for (@{$zoo{$animals}}) {
    	print OUT8 "\t$_";
	}
	print OUT8 "\n";
} #print the sequence info to file
close OUT8;

open SIMPLEOUT8, ">", "simple_combed_08_$input[0]";

&simplify;

for my $animals (sort keys %zoo) {
    print SIMPLEOUT8 "$animals";
    for (@{$zoo{$animals}}) {
    	print SIMPLEOUT8 "\t$_";
	}
	print SIMPLEOUT8 "\n";
} #print the sequence info to file

&clear_zoo;

print "Eight base words counted.\n";

open HOLD9, "<", "HOLD_09_$input[0]";
while (<HOLD9>) {
	my @line = split/\t/;
	my $sequence = shift @line;
	chomp $sequence;
	chomp @line;
	unless ($sequence =~ m/N/) {
		if (exists $zoo{$sequence}) {
			for (0..90) {
				$zoo{$sequence}[$_] = $zoo{$sequence}[$_] + $line[$_];
			}#add count to count for a particular contig sequence
		} else {
			for (0..90) {
				$zoo{$sequence}[$_] = $line[$_];
			}
		}
	}
	$zookeeper++; #add one to the tracker
	if (($zookeeper % 10000) == 0) {
		print "$zookeeper\n";
	}
}
close HOLD9;

open OUT9, ">", "combed_09_$input[0]";
for my $animals (sort keys %zoo) {
    print OUT9 "$animals";
    for (@{$zoo{$animals}}) {
    	print OUT9 "\t$_";
	}
	print OUT9 "\n";
} #print the sequence info to file
close OUT9;

open SIMPLEOUT9, ">", "simple_combed_09_$input[0]";

&simplify;

for my $animals (sort keys %zoo) {
    print SIMPLEOUT9 "$animals";
    for (@{$zoo{$animals}}) {
    	print SIMPLEOUT9 "\t$_";
	}
	print SIMPLEOUT9 "\n";
} #print the sequence info to file

&clear_zoo;

print "Nine base words counted.\n";

open HOLD10, "<", "HOLD_10_$input[0]";
while (<HOLD10>) {
	my @line = split/\t/;
	my $sequence = shift @line;
	chomp $sequence;
	chomp @line;
	unless ($sequence =~ m/N/) {
		if (exists $zoo{$sequence}) {
			for (0..90) {
				$zoo{$sequence}[$_] = $zoo{$sequence}[$_] + $line[$_];
			}#add count to count for a particular contig sequence
		} else {
			for (0..90) {
				$zoo{$sequence}[$_] = $line[$_];
			}
		}
	}
	$zookeeper++; #add one to the tracker
	if (($zookeeper % 10000) == 0) {
		print "$zookeeper\n";
	}
}
close HOLD10;

open OUT10, ">", "combed_10_$input[0]";
for my $animals (sort keys %zoo) {
    print OUT10 "$animals";
    for (@{$zoo{$animals}}) {
    	print OUT10 "\t$_";
	}
	print OUT10 "\n";
} #print the sequence info to file
close OUT10;

open SIMPLEOUT10, ">", "simple_combed_10_$input[0]";

&simplify;

for my $animals (sort keys %zoo) {
    print SIMPLEOUT10 "$animals";
    for (@{$zoo{$animals}}) {
    	print SIMPLEOUT10 "\t$_";
	}
	print SIMPLEOUT10 "\n";
} #print the sequence info to file

&clear_zoo;

print "Ten base words counted.\n";

sub clear_zoo {
	for (keys %zoo) {
		delete $zoo{$_};
	}
}

sub simplify {
	for my $sequence (sort keys %zoo) {
		my $reverse_compliment = reverse $sequence;
		$reverse_compliment =~ tr/ATCG/TAGC/;
		if (exists $zoo{$reverse_compliment}) {
			for (0..90) {
				$zoo{$reverse_compliment}[$_] = $zoo{$sequence}[$_] + $zoo{$reverse_compliment}[$_];
			}
			delete $zoo{$sequence};
		}
	}
} 