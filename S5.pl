#!/usr/bin/perl
#5positcounter.plx
#counts the number of occurance of "words" one-to-ten bases in length and counts which position (1-90) it occurs - fifth program (replaces sandbox.plx)
use warnings;
use strict;

print "Enter a sequence-counted file (\"seqcount_contig_136_blasted_merged_stripped_example.txt\"): ";

my $file = <STDIN>;
chomp $file;

my %zoo; #hash containing words for counting
my $reps; #amount of times a sequence is represented in the data
my @one; my @two; my @three; my @four; my @five; my @six; my @seven; my @eight; my @nine; my @ten;  #arrays to hold words of different sizes
my $zookeeper = 0; #tracks progress through data file
my $position = 0; #tracks possition of word
my @empty;
my $removed = 0; #tracks removed bases
my $word_length;

for (0..90) {
	$empty[$_] = 0;
}

open DATA, "<", "$file" or die "ERROR: Cannot open data file: $file.\n";

while (<DATA>) {
	$zookeeper++;
	my @line = split/\t/;
	$reps = pop @line;
	chomp $reps;
	my $sequence = pop @line; #split input line into copies of unique sequence and number of times represented in data
	$sequence =~ tr/agctn/AGCTN/; #make all bases uppercase
	@one = split//,$sequence; #split sequence into one-letter words
	$word_length = 1;
	$position = 0;
	for (@one) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@two = ($sequence =~ /../g); #split sequence into two-letter words with no shift
	$word_length = 2;
	$position = 0;
	for (@two) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@three = ($sequence =~ /.../g); #split sequence into three-letter words with no shift
	$word_length = 3;
	$position = 0;
	for (@three) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@four = ($sequence =~ /..../g); #split sequence into four-letter words with no shift
	$word_length = 4;
	$position = 0;
	for (@four) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@five = ($sequence =~ /...../g); #split sequence into five-letter words with no shift
	$word_length = 5;
	$position = 0;
	for (@five) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@six = ($sequence =~ /....../g); #split sequence into six-letter words with no shift
	$word_length = 6;
	$position = 0;
	for (@six) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@seven = ($sequence =~ /......./g); #split sequence into seven-letter words with no shift
	$word_length = 7;
	$position = 0;
	for (@seven) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@eight = ($sequence =~ /......../g); #split sequence into eight-letter words with no shift
	$word_length = 8;
	$position = 0;
	for (@eight) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@nine = ($sequence =~ /........./g); #split sequence into nine-letter words with no shift
	$word_length = 9;
	$position = 0;
	for (@nine) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@ten = ($sequence =~ /........../g); #split sequence into ten-letter words with no shift
	$word_length = 10;
	$position = 0;
	for (@ten) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	$sequence =~ s/^.//; #removes first base to check shift
	$removed++;
	@two = ($sequence =~ /../g); #split sequence into two-letter words with one-base shift
	$word_length = 2;
	$position = 0;
	for (@two) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@three = ($sequence =~ /.../g); #split sequence into three-letter words with one-base shift
	$word_length = 3;
	$position = 0;
	for (@three) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@four = ($sequence =~ /..../g); #split sequence into four-letter words with one-base shift
	$word_length = 4;
	$position = 0;
	for (@four) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@five = ($sequence =~ /...../g); #split sequence into five-letter words with one-base shift
	$word_length = 5;
	$position = 0;
	for (@five) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@six = ($sequence =~ /....../g); #split sequence into six-letter words with one-base shift
	$word_length = 6;
	$position = 0;
	for (@six) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@seven = ($sequence =~ /......./g); #split sequence into seven-letter words with one-base shift
	$word_length = 7;
	$position = 0;
	for (@seven) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@eight = ($sequence =~ /......../g); #split sequence into eight-letter words with one-base shift
	$word_length = 8;
	$position = 0;
	for (@eight) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@nine = ($sequence =~ /........./g); #split sequence into nine-letter words with one-base shift
	$word_length = 9;
	$position = 0;
	for (@nine) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@ten = ($sequence =~ /........../g); #split sequence into ten-letter words with one-base shift
	$word_length = 10;
	$position = 0;
	for (@ten) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	$sequence =~ s/^.//; #removes first base to check shift
	$removed++;
	@three = ($sequence =~ /.../g); #split sequence into three-letter words with two-base shift
	$word_length = 3;
	$position = 0;
	for (@three) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@four = ($sequence =~ /..../g); #split sequence into four-letter words with two-base shift
	$word_length = 4;
	$position = 0;
	for (@four) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@five = ($sequence =~ /...../g); #split sequence into five-letter words with two-base shift
	$word_length = 5;
	$position = 0;
	for (@five) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@six = ($sequence =~ /....../g); #split sequence into six-letter words with two-base shift
	$word_length = 6;
	$position = 0;
	for (@six) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@seven = ($sequence =~ /......./g); #split sequence into seven-letter words with two-base shift
	$word_length = 7;
	$position = 0;
	for (@seven) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@eight = ($sequence =~ /......../g); #split sequence into eight-letter words with two-base shift
	$word_length = 8;
	$position = 0;
	for (@eight) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@nine = ($sequence =~ /........./g); #split sequence into nine-letter words with two-base shift
	$word_length = 9;
	$position = 0;
	for (@nine) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@ten = ($sequence =~ /........../g); #split sequence into ten-letter words with two-base shift
	$word_length = 10;
	$position = 0;
	for (@ten) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	$sequence =~ s/^.//; #removes first base to check shift
	$removed++;
	@four = ($sequence =~ /..../g); #split sequence into four-letter words with three-base shift
	$word_length = 4;
	$position = 0;
	for (@four) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@five = ($sequence =~ /...../g); #split sequence into five-letter words with three-base shift
	$word_length = 5;
	$position = 0;
	for (@five) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@six = ($sequence =~ /....../g); #split sequence into six-letter words with three-base shift
	$word_length = 6;
	$position = 0;
	for (@six) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@seven = ($sequence =~ /......./g); #split sequence into seven-letter words with three-base shift
	$word_length = 7;
	$position = 0;
	for (@seven) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@eight = ($sequence =~ /......../g); #split sequence into eight-letter words with three-base shift
	$word_length = 8;
	$position = 0;
	for (@eight) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@nine = ($sequence =~ /........./g); #split sequence into nine-letter words with three-base shift
	$word_length = 9;
	$position = 0;
	for (@nine) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@ten = ($sequence =~ /........../g); #split sequence into ten-letter words with three-base shift
	$word_length = 10;
	$position = 0;
	for (@ten) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	$sequence =~ s/^.//; #removes first base to check shift
	$removed++;
	@five = ($sequence =~ /...../g); #split sequence into five-letter words with four-base shift
	$word_length = 5;
	$position = 0;
	for (@five) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@six = ($sequence =~ /....../g); #split sequence into six-letter words with four-base shift
	$word_length = 6;
	$position = 0;
	for (@six) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@seven = ($sequence =~ /......./g); #split sequence into seven-letter words with four-base shift
	$word_length = 7;
	$position = 0;
	for (@seven) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@eight = ($sequence =~ /......../g); #split sequence into eight-letter words with four-base shift
	$word_length = 8;
	$position = 0;
	for (@eight) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@nine = ($sequence =~ /........./g); #split sequence into nine-letter words with four-base shift
	$word_length = 9;
	$position = 0;
	for (@nine) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@ten = ($sequence =~ /........../g); #split sequence into ten-letter words with four-base shift
	$word_length = 10;
	$position = 0;
	for (@ten) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	$sequence =~ s/^.//; #removes first base to check shift
	$removed++;
	@six = ($sequence =~ /....../g); #split sequence into six-letter words with five-base shift
	$word_length = 6;
	$position = 0;
	for (@six) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@seven = ($sequence =~ /......./g); #split sequence into seven-letter words with five-base shift
	$word_length = 7;
	$position = 0;
	for (@seven) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@eight = ($sequence =~ /......../g); #split sequence into eight-letter words with five-base shift
	$word_length = 8;
	$position = 0;
	for (@eight) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@nine = ($sequence =~ /........./g); #split sequence into nine-letter words with five-base shift
	$word_length = 9;
	$position = 0;
	for (@nine) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@ten = ($sequence =~ /........../g); #split sequence into ten-letter words with five-base shift
	$word_length = 10;
	$position = 0;
	for (@ten) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	$sequence =~ s/^.//; #removes first base to check shift
	$removed++;
	@seven = ($sequence =~ /......./g); #split sequence into seven-letter words with six-base shift
	$word_length = 7;
	$position = 0;
	for (@seven) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@eight = ($sequence =~ /......../g); #split sequence into eight-letter words with six-base shift
	$word_length = 8;
	$position = 0;
	for (@eight) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@nine = ($sequence =~ /........./g); #split sequence into nine-letter words with six-base shift
	$word_length = 9;
	$position = 0;
	for (@nine) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@ten = ($sequence =~ /........../g); #split sequence into ten-letter words with six-base shift
	$word_length = 10;
	$position = 0;
	for (@ten) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	$sequence =~ s/^.//; #removes first base to check shift
	$removed++;
	@eight = ($sequence =~ /......../g); #split sequence into eight-letter words with seven-base shift
	$word_length = 8;
	$position = 0;
	for (@eight) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@nine = ($sequence =~ /........./g); #split sequence into nine-letter words with seven-base shift
	$word_length = 9;
	$position = 0;
	for (@nine) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@ten = ($sequence =~ /........../g); #split sequence into ten-letter words with seven-base shift
	$word_length = 10;
	$position = 0;
	for (@ten) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	$sequence =~ s/^.//; #removes first base to check shift
	$removed++;
	@nine = ($sequence =~ /........./g); #split sequence into nine-letter words with eight-base shift
	$word_length = 9;
	$position = 0;
	for (@nine) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	@ten = ($sequence =~ /........../g); #split sequence into ten-letter words with eight-base shift
	$word_length = 10;
	$position = 0;
	for (@ten) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	$sequence =~ s/^.//; #removes first base to check shift
	$removed++;
	@ten = ($sequence =~ /........../g); #split sequence into ten-letter words with nine-base shift
	$word_length = 10;
	$position = 0;
	for (@ten) {
		&add_to_zoo;
		$position = $position + $word_length;
	}
	$removed = 0;
	if ($zookeeper % 1000 == 0) {
		print "$zookeeper\n";
	}
}

close DATA;

sub add_to_zoo {
	my $tracker = $position + $removed;
	if (exists $zoo{$_}) {
		$zoo{$_}[90] = $zoo{$_}[90] + $reps; #add occurance to total
		$zoo{$_}[$tracker] = $zoo{$_}[$tracker] + $reps; #add occurance to position
	} else {
		$zoo{$_} = [@empty];
		$zoo{$_}[90] = $reps; #count first occurance of a word
		$zoo{$_}[$tracker] = $reps; #add occurance to position
	}
}

open OUT, ">", "wordcount_$file"; #output file
for my $animals (sort keys %zoo) {
    print OUT "$animals";
    for (@{$zoo{$animals}}) {
    	print OUT "\t$_";
	}
	print OUT "\n";
} #print the sequence info to file
close OUT; 