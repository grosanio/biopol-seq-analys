#!/usr/bin/perl
use warnings;
use strict;
use 5.010;
use FileHandle;

print "Enter control occurance file: ";

my $first_entry = <STDIN>;
chomp $first_entry;
my $control = FileHandle->new("$first_entry") or die "ERROR: $!";

print "Enter selection occurance file: ";

my $second_entry = <STDIN>;
chomp $second_entry;
my $selection = FileHandle->new("$second_entry") or die "ERROR: $!";

print "Enter reporting value (two time difference is 2): ";
my $value = <STDIN>;
chomp $value;

print "Enter reporting cutoff (if you entered 2 above, 1 would find values no more than three time difference): ";

my $cutoff = <STDIN>;
chomp $cutoff;

my $outer;
if ($cutoff) {
	$outer = $value + $cutoff;
} else {
	$outer = $value;
	
}
my $i_value = 1/$value;
my $i_outer = 1/$outer;


my ($SL, $selection_word, $selection_occurance, $CL, $control_word, $control_occurance);
my (@selection_line, @control_line);
my %catch;
my $binary = 0;

while (!$control->eof() || !$selection->eof()) {
	$SL = $selection->getline();
	@selection_line = split/\t/,$SL;
	$selection_word = $selection_line[0];
	chomp $selection_word;
	$selection_occurance = $selection_line[1];
	chomp $selection_occurance;
	$CL = $control->getline();
	@control_line = split/\t/,$CL;
	$control_word = $control_line[0];
	chomp $control_word;
	$control_occurance = $control_line[1];
	chomp $control_occurance;
	if ($control_word =! $selection_word) {
		print "Error: Lines do not match between files.\n";
		exit;
	}
	my $ratio = $selection_occurance / $control_occurance;
	if ((($ratio >= $value)&&($ratio <= $outer))or(($ratio >= $i_outer)&&($ratio <= $i_value))) {
		my $difference = abs($ratio - $value);
		my $i_difference = abs($ratio - $i_value);
		my $distance;
		if ($difference >= $i_difference) {
			$distance = $i_difference;
		} elsif ($difference < $i_difference) {
			$distance = $difference;
		}
		$catch{$selection_word} = [$distance, $ratio, $selection_occurance, $control_occurance];
		$binary = 1;
	}
}

$control->close();
$selection->close();
unless ($binary == 0) {
	my $name = "$value"."-"."$outer"."$second_entry";
	my $out = FileHandle->new(">ratio_$name.txt") or die "ERROR: $!";
	for my $key (sort {$catch{$a}->[0] cmp $catch{$b}->[0] } keys %catch) {
		$out->print("$key\t$catch{$key}[1]\t$catch{$key}[2]\t$catch{$key}[3]\n");
	}
	$out->close();
} 