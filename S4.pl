#!/usr/bin/perl
#uniquerv2.plx
#counts the number of occurance of complete contigs - sixth program
use warnings;
use strict;

print "Enter lengthed contig files of one size (\"contig_136_blasted_merged_stripped_example.txt\"): ";

my $entry = <STDIN>;
chomp $entry;
my @all_files = split(' ', $entry);

print "Do you want to add known ends onto the trimmed sequences? (yes/no): ";
$entry = <STDIN>;
chomp $entry;
my $ends = 0;
if ($entry =~ m/(Y|y)(E|e)*(S|s)*/) {
	$ends = 1;
}

for (@all_files) {
	my $file = "$_";
	open DATA, "<", "$file" or die "ERROR: No file(s) of that name exists in this folder.\n";	#input file

	my %zoo; #hash for counting
	my $zookeeper; #counts the number of contigs read
	my $animals; #keys for printout
	my $mates; #values for printout

	while (<DATA>) {
		my @line = split/\t/;
		my $sequence = pop @line;
		chomp $sequence;
		if ($ends == 1) {
			$sequence = "AGCTTCAGCGATGAGATGAG"."$sequence"."TATCTGGTGGGAAACAAGCT";
		}
		if (exists $zoo{$sequence}) {
			$zoo{$sequence} = $zoo{$sequence} + 1; #add one to count for a particular contig sequence
		} else {
			$zoo{$sequence} = 1; #count first occurance of a particular contig sequence
		}
		$zookeeper++; #add one to the tracker
	}

	close DATA;

	if ($ends == 1) {
		$file = "ends_"."$file";
	}
	open OUT, ">", "seqcounts_$file"; #output file
	while (($animals,$mates) = each %zoo) {
		print OUT "$animals\t$mates\n";
	} #print the sequence info to file
	close OUT;

	open BRIEF, ">", "brief_seqcounts_$file"; #just to see the overview if run on cluster
	my $cages = scalar keys %zoo; #count number of sequences
	print BRIEF "$zookeeper sequences sorted into $cages\n";
	close BRIEF;

	print "$zookeeper sequences sorted into $cages\n";
} 