#!/usr/bin/perl
#barcoder.plx
#sorts intermediate sequences by barcodes
use warnings;
use strict;

my %GroupA = ('ANCACG',0,'ANCACN',0,'ANCANG',0,'ANCANN',0,'ANCNCG',0,'ANCNCN',0,'ANCNNG',0,'ANCNNN',0,'ANNACG',0,'ANNACN',0,'ANNANG',0,'ANNANN',0,'ANNNCG',0,'ANNNCN',0,'ANNNNG',0,'ATCACG',0,'ATCACN',0,'ATCANG',0,'ATCANN',0,'ATCNCG',0,'ATCNCN',0,'ATCNNG',0,'ATCNNN',0,'ATNACG',0,'ATNACN',0,'ATNANG',0,'ATNANN',0,'ATNNCG',0,'ATNNCN',0,'ATNNNG',0,'ATNNNN',0,'NNCACG',0,'NNCACN',0,'NNCANG',0,'NNCANN',0,'NNCNCG',0,'NNCNCN',0,'NNCNNG',0,'NNCNNN',0,'NNNACG',0,'NNNACN',0,'NNNANG',0,'NNNNCG',0,'NNNNCN',0,'NNNNNG',0,'NTCACG',0,'NTCACN',0,'NTCANG',0,'NTCANN',0,'NTCNCG',0,'NTCNCN',0,'NTCNNG',0,'NTCNNN',0,'NTNACG',0,'NTNACN',0,'NTNANG',0,'NTNANN',0,'NTNNCG',0,'NTNNCN',0,'NTNNNG',0,);
my %GroupB = ('CGANGN',0,'CGANGT',0,'CGANNN',0,'CGANNT',0,'CGATGN',0,'CGATGT',0,'CGATNN',0,'CGATNT',0,'CGNNGN',0,'CGNNGT',0,'CGNNNN',0,'CGNNNT',0,'CGNTGN',0,'CGNTGT',0,'CGNTNN',0,'CGNTNT',0,'CNANGN',0,'CNANGT',0,'CNANNN',0,'CNANNT',0,'CNATGN',0,'CNATGT',0,'CNATNN',0,'CNATNT',0,'CNNNGN',0,'CNNNGT',0,'CNNNNT',0,'CNNTGN',0,'CNNTGT',0,'CNNTNN',0,'CNNTNT',0,'NGANGN',0,'NGANGT',0,'NGANNN',0,'NGANNT',0,'NGATGN',0,'NGATGT',0,'NGATNN',0,'NGATNT',0,'NGNNGN',0,'NGNNGT',0,'NGNNNN',0,'NGNNNT',0,'NGNTGN',0,'NGNTGT',0,'NGNTNN',0,'NGNTNT',0);
my %GroupC = ('NNAGGC',0,'NNAGGN',0,'NNAGNC',0,'NNAGNN',0,'NNANGC',0,'NNANNC',0,'NNNGGC',0,'NNNGGN',0,'NNNGNC',0,'NNNGNN',0,'NNNNGC',0,'NTAGGC',0,'NTAGGN',0,'NTAGNC',0,'NTAGNN',0,'NTANGC',0,'NTANGN',0,'NTANNC',0,'NTANNN',0,'NTNGGC',0,'NTNGGN',0,'NTNGNC',0,'NTNGNN',0,'NTNNGC',0,'NTNNGN',0,'NTNNNC',0,'TNAGGC',0,'TNAGGN',0,'TNAGNC',0,'TNAGNN',0,'TNANGC',0,'TNANGN',0,'TNANNC',0,'TNANNN',0,'TNNGGC',0,'TNNGGN',0,'TNNGNC',0,'TNNGNN',0,'TNNNGC',0,'TNNNGN',0,'TNNNNC',0,'TNNNNN',0,'TTAGGC',0,'TTAGGN',0,'TTAGNC',0,'TTAGNN',0,'TTANGC',0,'TTANGN',0,'TTANNC',0,'TTANNN',0,'TTNGGC',0,'TTNGGN',0,'TTNGNC',0,'TTNGNN',0,'TTNNGC',0,'TTNNGN',0,'TTNNNC',0,'TTNNNN',0);
my %GroupE = ('ACANGN',0,'ACANGT',0,'ACANNN',0,'ACANNT',0,'ACATGN',0,'ACATGT',0,'ACATNN',0,'ACATNT',0,'ACNNGN',0,'ACNNGT',0,'ACNNNN',0,'ACNNNT',0,'ACNTGN',0,'ACNTGT',0,'ACNTNN',0,'ACNTNT',0,'ANANGN',0,'ANANGT',0,'ANANNN',0,'ANANNT',0,'ANATGN',0,'ANATGT',0,'ANATNN',0,'ANATNT',0,'ANNNGN',0,'ANNNGT',0,'ANNNNT',0,'ANNTGN',0,'ANNTGT',0,'ANNTNN',0,'ANNTNT',0,'NCANGN',0,'NCANGT',0,'NCANNN',0,'NCANNT',0,'NCATGN',0,'NCATGT',0,'NCATNN',0,'NCATNT',0,'NCNNGN',0,'NCNNGT',0,'NCNNNN',0,'NCNNNT',0,'NCNTGN',0,'NCNTGT',0,'NCNTNN',0,'NCNTNT',0);
my %GroupG = ('CAGANC',0,'CAGANN',0,'CAGATC',0,'CAGATN',0,'CAGNNC',0,'CAGNNN',0,'CAGNTC',0,'CAGNTN',0,'CANANC',0,'CANANN',0,'CANATC',0,'CANATN',0,'CANNNC',0,'CANNNN',0,'CANNTC',0,'CANNTN',0,'CNGANC',0,'CNGANN',0,'CNGATC',0,'CNGATN',0,'CNGNNC',0,'CNGNNN',0,'CNGNTC',0,'CNGNTN',0,'CNNANC',0,'CNNANN',0,'CNNATC',0,'CNNATN',0,'CNNNNC',0,'CNNNTC',0,'CNNNTN',0,'NAGANC',0,'NAGANN',0,'NAGATC',0,'NAGATN',0,'NAGNNC',0,'NAGNNN',0,'NAGNTC',0,'NAGNTN',0,'NANANC',0,'NANANN',0,'NANATC',0,'NANATN',0,'NANNNC',0,'NANNNN',0,'NANNTC',0,'NANNTN',0,'NNGANC',0,'NNGANN',0,'NNGATC',0,'NNGATN',0,'NNGNNC',0,'NNGNNN',0,'NNGNTC',0,'NNGNTN',0,'NNNANC',0,'NNNATC',0,'NNNATN',0,'NNNNTC',0,'NNNNTN',0);
my %Trash = ('ANNNNN',0,'CNNNNN',0,'NNANGN',0,'NNANGT',0,'NNANNN',0,'NNANNT',0,'NNATGN',0,'NNATGT',0,'NNATNN',0,'NNATNT',0,'NNNANN',0,'NNNNGN',0,'NNNNGT',0,'NNNNNC',0,'NNNNNT',0,'NNNTGN',0,'NNNTGT',0,'NNNTNN',0,'NNNTNT',0,'NTNNNN',0,'NNNNNN',0);
my %GroupAprime = ('CGNGAN',0,'CGNGAT',0,'CGNGNN',0,'CGNGNT',0,'CGNNAN',0,'CGNNAT',0,'CGNNNN',0,'CGNNNT',0,'CGTGAN',0,'CGTGAT',0,'CGTGNN',0,'CGTGNT',0,'CGTNAN',0,'CGTNAT',0,'CGTNNN',0,'CGTNNT',0,'CNNGAN',0,'CNNGAT',0,'CNNGNN',0,'CNNGNT',0,'CNNNAN',0,'CNNNAT',0,'CNNNNN',0,'CNNNNT',0,'CNTGAN',0,'CNTGAT',0,'CNTGNN',0,'CNTGNT',0,'CNTNAN',0,'CNTNAT',0,'CNTNNN',0,'CNTNNT',0,'NGNGAN',0,'NGNGAT',0,'NGNGNN',0,'NGNGNT',0,'NGNNAN',0,'NGNNAT',0,'NGNNNN',0,'NGNNNT',0,'NGTGAN',0,'NGTGAT',0,'NGTGNN',0,'NGTGNT',0,'NGTNAN',0,'NGTNAT',0,'NGTNNN',0,'NGTNNT',0,'NNNGAN',0,'NNNGAT',0,'NNNGNN',0,'NNNGNT',0,'NNNNAT',0,'NNTGAN',0,'NNTGAT',0,'NNTGNN',0,'NNTGNT',0,'NNTNAN',0,'NNTNAT',0,'NNTNNT',0,);
my %GroupBprime = ('ACANCG',0,'ACANCN',0,'ACANNG',0,'ACATCG',0,'ACATCN',0,'ACATNG',0,'ACNNCG',0,'ACNNCN',0,'ACNNNG',0,'ACNTCG',0,'ACNTCN',0,'ACNTNG',0,'ANANCG',0,'ANANCN',0,'ANANNG',0,'ANATCG',0,'ANATCN',0,'ANATNG',0,'ANNNCG',0,'ANNNCN',0,'ANNNNG',0,'ANNTCG',0,'ANNTCN',0,'ANNTNG',0,'NCANCG',0,'NCANCN',0,'NCANNG',0,'NCATCG',0,'NCATCN',0,'NCATNG',0,'NCNNCG',0,'NCNNCN',0,'NCNNNG',0,'NCNTCG',0,'NCNTCN',0,'NCNTNG',0,'NNANCG',0,'NNANCN',0,'NNANNG',0,'NNATCG',0,'NNATCN',0,'NNATNG',0,'NNNNCG',0,'NNNNCN',0,'NNNTCG',0,'NNNTCN',0,'NNNTNG',0);
my %GroupCprime = ('GCCNAA',0,'GCCNAN',0,'GCCNNA',0,'GCCNNN',0,'GCCTAA',0,'GCCTAN',0,'GCCTNA',0,'GCCTNN',0,'GCNNAA',0,'GCNNAN',0,'GCNNNA',0,'GCNNNN',0,'GCNTAA',0,'GCNTAN',0,'GCNTNA',0,'GCNTNN',0,'GNCNAA',0,'GNCNAN',0,'GNCNNA',0,'GNCNNN',0,'GNCTAA',0,'GNCTAN',0,'GNCTNA',0,'GNCTNN',0,'GNNNAA',0,'GNNNAN',0,'GNNNNA',0,'GNNTAA',0,'GNNTAN',0,'GNNTNA',0,'GNNTNN',0,'NCCNAA',0,'NCCNAN',0,'NCCNNA',0,'NCCNNN',0,'NCCTAA',0,'NCCTAN',0,'NCCTNA',0,'NCCTNN',0,'NCNNAA',0,'NCNNAN',0,'NCNNNA',0,'NCNTAA',0,'NCNTAN',0,'NCNTNA',0,'NNCNAA',0,'NNCNAN',0,'NNCNNA',0,'NNCNNN',0,'NNCTAA',0,'NNCTAN',0,'NNCTNA',0,'NNCTNN',0,'NNNNAA',0,'NNNNNA',0,'NNNTAA',0,'NNNTAN',0,'NNNTNA',0);
my %GroupEprime = ('ACANGN',0,'ACANGT',0,'ACANNT',0,'ACATGN',0,'ACATGT',0,'ACATNT',0,'ACNNGN',0,'ACNNGT',0,'ACNNNT',0,'ACNTGN',0,'ACNTGT',0,'ACNTNT',0,'ANANGN',0,'ANANGT',0,'ANANNT',0,'ANATGN',0,'ANATGT',0,'ANATNT',0,'ANNNGN',0,'ANNNGT',0,'ANNNNT',0,'ANNTGN',0,'ANNTGT',0,'ANNTNT',0,'NCANGN',0,'NCANGT',0,'NCANNT',0,'NCATGN',0,'NCATGT',0,'NCATNT',0,'NCNNGN',0,'NCNNGT',0,'NCNNNT',0,'NCNTGN',0,'NCNTGT',0,'NCNTNT',0,'NNANGN',0,'NNANGT',0,'NNANNT',0,'NNATGN',0,'NNATGT',0,'NNATNT',0,'NNNNGN',0,'NNNNGT',0,'NNNTGN',0,'NNNTGT',0,'NNNTNT',0);
my %GroupGprime = ('GANCNG',0,'GANCNN',0,'GANCTG',0,'GANCTN',0,'GANNNG',0,'GANNNN',0,'GANNTG',0,'GANNTN',0,'GATCNG',0,'GATCNN',0,'GATCTG',0,'GATCTN',0,'GATNNG',0,'GATNNN',0,'GATNTG',0,'GATNTN',0,'GNNCNG',0,'GNNCNN',0,'GNNCTG',0,'GNNCTN',0,'GNNNNG',0,'GNNNTG',0,'GNNNTN',0,'GNTCNG',0,'GNTCNN',0,'GNTCTG',0,'GNTCTN',0,'GNTNNG',0,'GNTNNN',0,'GNTNTG',0,'GNTNTN',0,'NANCNG',0,'NANCNN',0,'NANCTG',0,'NANCTN',0,'NANNNG',0,'NANNNN',0,'NANNTG',0,'NANNTN',0,'NATCNG',0,'NATCNN',0,'NATCTG',0,'NATCTN',0,'NATNNG',0,'NATNNN',0,'NATNTG',0,'NATNTN',0,'NNNCNG',0,'NNNCNN',0,'NNNCTG',0,'NNNCTN',0,'NNNNTG',0,'NNNNTN',0,'NNTCNG',0,'NNTCNN',0,'NNTCTG',0,'NNTCTN',0,'NNTNNG',0,'NNTNTG',0,'NNTNTN',0,);
my %Trashprime = ('ACANNN',0,'ACATNN',0,'ACNNNN',0,'ACNTNN',0,'ANANNN',0,'ANATNN',0,'ANNNNN',0,'ANNTNN',0,'GNNNNN',0,'NCANNN',0,'NCATNN',0,'NCNNNN',0,'NCNTNN',0,'NNANNN',0,'NNATNN',0,'NNNNAN',0,'NNNNNG',0,'NNNNNT',0,'NNNTNN',0,'NNTNNN',0,'NNNNNN',0);																																					

my $sort_attempt = my $sorted = my $first = my $second = my $third = my $fourth = my $fifth = my $trash = 0;
my $beadid;
my $N90;

print "Enter a mixed contig file: ";
my $file = <STDIN>;
chomp $file;

open FIRST, ">", "firstround_$file" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
open SECOND, ">", "secondround_$file" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
open THIRD, ">", "thirdround_$file" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
open FOURTH, ">", "fourthround_$file" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
open FIFTH, ">", "fifthround_$file" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created
open TRASH, ">", "trash_$file" or die "ERROR: Cannot create alignment output file.\n";	#output file for alignment sequences created


open DATA, "<", "$file" or die "ERROR: No file(s) of that name exists in this folder.\n";
	while (<DATA>) {
		my @line = split/\t/;
		$beadid = $line[0];
		my $sequence = $line[1];
		my $barcodeF = substr $sequence, 0,6;
		my $barcodeB = substr $sequence, -6, 6;
		$N90 = substr $sequence, 24, 90;

		if (exists $Trash{$barcodeF} && exists $Trashprime{$barcodeF}) {
			if (exists $Trash{$barcodeB} && exists $Trashprime{$barcodeB}) {
				&trash;
			}elsif (exists $GroupE{$barcodeB} || exists $GroupEprime{$barcodeB}) {
				&GroupE;
			}elsif (exists $GroupB{$barcodeB} || exists $GroupBprime{$barcodeB}) {
				&GroupB;		
			}elsif (exists $GroupC{$barcodeB} || exists $GroupCprime{$barcodeB}) {
				&GroupC;
			}elsif (exists $GroupA{$barcodeB} || exists $GroupAprime{$barcodeB}) {
				&GroupA;		
			}elsif (exists $GroupG{$barcodeB} || exists $GroupGprime{$barcodeB}) {
				&GroupG;
			}else {
				&trash;
			}		
		}elsif (exists $GroupE{$barcodeF} || exists $GroupEprime{$barcodeF}) {
			&GroupE;
		}elsif (exists $GroupB{$barcodeF} || exists $GroupBprime{$barcodeF}) {
			&GroupB;		
		}elsif (exists $GroupC{$barcodeF} || exists $GroupCprime{$barcodeF}) {
			&GroupC;
		}elsif (exists $GroupA{$barcodeF} || exists $GroupAprime{$barcodeF}) {
			&GroupA;		
		}elsif (exists $GroupG{$barcodeF} || exists $GroupGprime{$barcodeF}) {
			&GroupG;
		}else {
			&trash;
		}	
	$sort_attempt++;
	if (($sort_attempt % 1000) == 0) {
		print "$sort_attempt\n";
	}
}
print "$sorted sequences out of $sort_attempt sequences were sorted.\n$first sequences were from the first round of selection.\n$second sequences were from the second round of selection.\n$third sequences were from the third round of selection.\n$fourth sequences were from the fourth round of selection.\$fifth sequences were from the fifth round of selection.\$trash sequences were unsortable and were thrown out.\n";

close DATA;
close FIRST;
close SECOND;
close THIRD;
close FOURTH;
close FIFTH;
close TRASH;
		
sub GroupA	{
	print FIRST "$beadid\t$N90\n";
	$first++;
	$sorted++;
}
sub GroupB	{
	print SECOND "$beadid\t$N90\n";
	$second++;
	$sorted++;
}
sub GroupC	{
	print THIRD "$beadid\t$N90\n";
	$third++;
	$sorted++;
}
sub GroupE	{
	print FOURTH "$beadid\t$N90\n";
	$fourth++;
	$sorted++;
}
sub GroupG	{
	print FIFTH "$beadid\t$N90\n";
	$fifth++;
	$sorted++;
}
sub trash {
	print TRASH "$beadid\t$N90\n";
	$trash++;
}