#!/usr/bin/perl
use warnings;
use strict;

my $glo0 = "[G|C][G|C]";

print "Enter sequence-counted files (\"seqcount_contig_136_contig_136_FIRSTexample.txt seqcount_contig_136_contig_136_SECONDexample.txt seqcount_contig_136_contig_136_THIRDexample.txt\"): ";


my $files = <STDIN>;
chomp $files;
my @input = split / /, $files;
my $name = $input[0];
my $tracker = 0;
#my %cggapsO = ('sequences without C/G dinucleotide', 0, 'sequences with only one C/G dinucleotide', 0);
my %cggapsN = ('sequences without C/G dinucleotide', 0, 'sequences with only one C/G dinucleotide', 0);


for my $file (@input) {
	open DATA, "<", "$file" or die "ERROR: $file does not exists in this folder.\n";
	close DATA;
}

#open OUTO, ">", "cggap_overlaps_separated_$name";
#print OUTO "Sequence\tNearest Next Dinucleotide\n";
open OUTN, ">", "cggap_nonoverlaps_separated_$name";
print OUTN "Sequence\tNearest Next Dinucleotide\n";

for my $file (@input) {
	open DATA, "<", "$file" or die "ERROR: No file(s) of that name exists in this folder.\n";
	while (<DATA>) {
#		my $goneO = 0;
		my $goneN = -1;
		my @line = split/\s/;
		my $reps = $line[1];
		chomp $reps;
		$tracker = $tracker + $reps;
		my $sequence = $line[0]; #split input line into copies of unique sequence and number of times represented in data
#		print OUTO "$sequence\n";
		print OUTN "$sequence\t";
		my $holdO = my $holdN = $sequence;
#		my @foundO;
		my @foundN;
#		while ($holdO =~ /$glo0/){
#			my $removeO = $-[0] + 1;
#			$goneO = $goneO + $removeO;
#			push @foundO, $goneO;
#			$holdO =~ s/^.{$removeO}//;
#		}
		while ($holdN =~ /$glo0/){
			my $removeN = $-[0] + 2;
			$goneN = $goneN + $removeN;
			push @foundN, $goneN;			
			$holdN =~ s/^.{$removeN}//;
		}
#		my $numberO = scalar(@foundO);
		my $numberN = scalar(@foundN);
#		my @distanceO;
		my @distanceN;
#		if ($numberO == 1 ) {
#			$cggapsO{'sequences with only one C/G dinucleotide'} = $cggapsO{'sequences with only one C/G dinucleotide'} + $reps;
#		} elsif ($numberO == 0 ) {
#			$cggapsO{'sequences without C/G dinucleotide'} = $cggapsO{'sequences without C/G dinucleotide'} + $reps;
#		} else {
#			while ($numberO > 1){
#				my $largest = pop @foundO;
#				my $next = $foundO[$#foundO];
#				my $difference = $largest - $next;
#				push @distanceO, $difference;
#				$numberO--;
#			}
#		}
#		for (@distanceO) {
#			if (exists($cggapsO{$_})) {
#				$cggapsO{$_} = $cggapsO{$_} + $reps;
#			}else {
#				$cggapsO{$_} = $reps;
#			}
#		}
		if ($numberN == 1 ) {
			$cggapsN{'sequences with only one C/G dinucleotide'} = $cggapsN{'sequences with only one C/G dinucleotide'} + $reps;
		} elsif ($numberN == 0 ) {
			$cggapsN{'sequences without C/G dinucleotide'} = $cggapsN{'sequences without C/G dinucleotide'} + $reps;
		} else {
			while ($numberN > 1){
				my $largest = pop @foundN;
				my $next = $foundN[$#foundN];
				my $difference = $largest - $next;
				push @distanceN, $difference;
				$numberN--;
			}
		}
		for (0..($reps-1)) {
#			print OUTO "@distanceO\n";
			print OUTN "@distanceN\n";
		}
		for (@distanceN) {
			if (exists($cggapsN{$_})) {
				$cggapsN{$_} = $cggapsN{$_} + $reps;
			}else {
				$cggapsN{$_} = $reps;
			}
		}
		if ($tracker % 10000 == 0) {
			print "$tracker\n";
		}
	}
}

#close OUTO;
close OUTN;

#open BRIEFO, ">", "cggap_overlaps_all_motifpermolecule_$name";
#print BRIEFO "Overlapping Gap\tOccurrences\n";
#for my $key ( sort keys %cggapsO) {
#           print BRIEFO "$key\t$cggapsO{$key}\n";
#}
#close BRIEFO;

open BRIEFN, ">", "cggap_nonoverlaps_all_motifpermolecule_$name";
print BRIEFN "Non-Overlapping Gap\tOccurrences\n";
for my $key ( sort keys %cggapsN) {
           print BRIEFN "$key\t$cggapsN{$key}\n";
}
close BRIEFN;